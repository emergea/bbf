(function ()
{
    'use strict';

    angular
        .module('fuse')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope, $timeout, $state, AuthService, api)
    {
        // Activate loading indicator
        var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function (event, toState)
        {
            $rootScope.loadingProgress = true;
            if (!AuthService.isAuthenticated)
            {
                // Add all state that are known
                if (toState.name != 'app.pages_auth_login' && toState.name != 'app.pages_auth_register')
                {                    
                    event.preventDefault();
                    $state.go('app.pages_auth_login');
                }                
                
                
            }
            else
            {
                if (!$rootScope.name) 
                {
                    api.user.getUsers.get
                    (
                        //Success response
                        function (response)
                        {
                            $rootScope.name = response.name;
                            event.preventDefault();
                            
                        },

                        //Failure response
                        function (response) 
                        {
                            console.error(response);
                            AuthService.logout();
                            $state.go('app.pages_auth_login');
                            event.preventDefault();
                        }
                    );
                }
            }           
        });

        // De-activate loading indicator
        var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
        {
            $timeout(function ()
            {
                $rootScope.loadingProgress = false;
            });
        });

        // Store state in the root scope for easy access
        $rootScope.state = $state;

        // Cleanup
        $rootScope.$on('$destroy', function ()
        {
            stateChangeStartEvent();
            stateChangeSuccessEvent();
        });
    }
})();