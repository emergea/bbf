(function ()
{
    'use strict';

    angular
        .module('fuse')
        .factory('api', apiService);

    /** @ngInject */
    function apiService($resource, $http, $rootScope)
    {
        /**
         * You can use this service to define your API urls. The "api" service
         * is designed to work in parallel with "apiResolver" service which you can
         * find in the "app/core/services/api-resolver.service.js" file.
         *
         * You can structure your API urls whatever the way you want to structure them.
         * You can either use very simple definitions, or you can use multi-dimensional
         * objects.
         *
         * Here's a very simple API url definition example:
         *
         *      api.getBlogList = $resource('http://api.example.com/getBlogList');
         *
         * While this is a perfectly valid $resource definition, most of the time you will
         * find yourself in a more complex situation where you want url parameters:
         *
         *      api.getBlogById = $resource('http://api.example.com/blog/:id', {id: '@id'});
         *
         * You can also define your custom methods. Custom method definitions allow you to
         * add hardcoded parameters to your API calls that you want to sent every time you
         * make that API call:
         *
         *      api.getBlogById = $resource('http://api.example.com/blog/:id', {id: '@id'}, {
         *         'getFromHomeCategory' : {method: 'GET', params: {blogCategory: 'home'}}
         *      });
         *
         * In addition to these definitions, you can also create multi-dimensional objects.
         * They are nothing to do with the $resource object, it's just a more convenient
         * way that we have created for you to packing your related API urls together:
         *
         *      api.blog = {
         *                   list     : $resource('http://api.example.com/blog'),
         *                   getById  : $resource('http://api.example.com/blog/:id', {id: '@id'}),
         *                   getByDate: $resource('http://api.example.com/blog/:date', {id: '@date'}, {
         *                       get: {
         *                            method: 'GET',
         *                            params: {
         *                                getByDate: true
         *                            }
         *                       }
         *                   })
         *       }
         *
         * If you look at the last example from above, we overrode the 'get' method to put a
         * hardcoded parameter. Now every time we make the "getByDate" call, the {getByDate: true}
         * object will also be sent along with whatever data we are sending.
         *
         * All the above methods are using standard $resource service. You can learn more about
         * it at: https://docs.angularjs.org/api/ngResource/service/$resource
         *
         * -----
         *
         * After you defined your API urls, you can use them in Controllers, Services and even
         * in the UIRouter state definitions.
         *
         * If we use the last example from above, you can do an API call in your Controllers and
         * Services like this:
         *
         *      function MyController (api)
         *      {
         *          // Get the blog list
         *          api.blog.list.get({},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *
         *          // Get the blog with the id of 3
         *          var id = 3;
         *          api.blog.getById.get({'id': id},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *
         *          // Get the blog with the date by using custom defined method
         *          var date = 112314232132;
         *          api.blog.getByDate.get({'date': date},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *      }
         *
         * Because we are directly using $resource service, all your API calls will return a
         * $promise object.
         *
         * --
         *
         * If you want to do the same calls in your UI Router state definitions, you need to use
         * "apiResolver" service we have prepared for you:
         *
         *      $stateProvider.state('app.blog', {
         *          url      : '/blog',
         *          views    : {
         *               'content@app': {
         *                   templateUrl: 'app/main/apps/blog/blog.html',
         *                   controller : 'BlogController as vm'
         *               }
         *          },
         *          resolve  : {
         *              Blog: function (apiResolver)
         *              {
         *                  return apiResolver.resolve('blog.list@get');
         *              }
         *          }
         *      });
         *
         *  You can even use parameters with apiResolver service:
         *
         *      $stateProvider.state('app.blog.show', {
         *          url      : '/blog/:id',
         *          views    : {
         *               'content@app': {
         *                   templateUrl: 'app/main/apps/blog/blog.html',
         *                   controller : 'BlogController as vm'
         *               }
         *          },
         *          resolve  : {
         *              Blog: function (apiResolver, $stateParams)
         *              {
         *                  return apiResolver.resolve('blog.getById@get', {'id': $stateParams.id);
         *              }
         *          }
         *      });
         *
       *  And the "Blog" object will be availble in your BlogController:
         *
         *      function BlogController(Blog)
         *      {
         *          var vm = this;
         *
         *          // Data
         *          vm.blog = Blog;
         *
         *          ...
         *      }
         */

        var api = {};        

        // TODO: refactor as soon as Danie, brent and Nakkiran.
        // operationMode.orchestra = false;
        // operationMode.mongo = true;


        // Base Url
        
        // Prod
        // api.baseUrl = 'http://192.168.0.25:8080';
        // api.baseUrl = 'http://52.40.64.28:8080'; //AWS
        // api.mongoUrl = 'http://192.168.0.132:4000';
        
        // Dev
        api.baseUrl = 'http://localhost:3000';
        $rootScope.$broadcast('set-baseUrl',{ "baseUrl": api.baseUrl });
        api.mongoUrl = 'http://localhost:4000';
        $rootScope.$broadcast('set-mongoUrl',{ "mongoUrl": api.mongoUrl });
        api.central = true;
        $rootScope.$broadcast('set-central',{ "central": api.central });
        api.distribution = false;
        $rootScope.$broadcast('set-distribution',{ "distribution": api.distribution });
        //TODO: Should this be a setting in mongo rather that is editable by the user?
        api.maxTicketNumberCount = 500;
        $rootScope.$broadcast('set-maxTicketNumberCount',{ "maxTicketNumberCount": api.maxTicketNumberCount });

        $http.defaults.headers.common['Authorization'] = 'Basic ' + 'c3VwZXJhZG1pbjp1bGFu';

        api.game = {

            GamesData: $resource('http://52.88.85.116:4000/mobileGames', {},
                {
                    get:
                    {
                        method: 'GET',
                        isArray: true
                    },
                    post:
                    {
                        method: 'POST',
                        isArray: false
                    }
                }),
            GameData: $resource('http://52.88.85.116:4000/mobileGames/:gameId', {},
                {
                    get:
                    {
                        method: 'GET',
                        isArray: false
                    },
                    put:
                    {
                        method: 'PUT',
                        isArray: false
                    },
                    delete:
                    {
                        method: 'DELETE',
                        isArray: false
                    }
                })
        };

        //api.mongomobile = {
        //        getBranch: $resource('http://localhost:4000/branch/:id', { id: '@id' }),
        //        getBranchQueques: $resource('http://localhost:4000/queues/', {}),
        //        getBranchQueque: $resource('http://localhost:4000/queues/:queueId', { queueId: '@queueId' }),
        //        getBranchServices: $resource('http://localhost:4000/services/', {}),
        //        getBranchVisit: $resource('http://localhost:4000/visits/:visitId', { visitId: '@visitId' }),
        //        createBranchVisit: $resource('http://localhost:4000/visits', {}, {
        //            post: {
        //                method: 'POST',
        //                headers: { 'content-Type': 'Application/Json' }
        //            }
        //        }),

        //        saveNewCustomer: $resource('http://localhost:4000/customers/', {}, {
        //            save: {
        //                method: 'POST',
        //                isArray: true,
        //                headers: { 'content-Type': 'Application/Json' }
        //            }
        //        })
            
        //};

        api.mobile =
        {
            getBranch : $resource(api.baseUrl + '/rest/entrypoint/branches/:branchId', {}),
            getBranchQueques: $resource(api.baseUrl + '/rest/entrypoint/branches/:branchId/queues/', {}),
            getBranchQueque: $resource(api.baseUrl + '/rest/entrypoint/branches/:branchId/queues/:queueId', {}),
            getBranchServices: $resource(api.baseUrl + '/rest/entrypoint/branches/:branchId/services/', {}),            
            getBranchVisit: $resource(api.baseUrl + '/rest/entrypoint/branches/:branchId/visits/:visitId', {}),
            createBranchVisit: $resource(api.baseUrl + '/rest/entrypoint/branches/:branchId/entryPoints/:entryPointId/visits', { services: '@services' }, {
                post: {
                    method: 'POST'
                }
            }),

            saveNewCustomer: $resource('/rest/entrypoint/customers/', {}, {
                save: {
                    method: 'POST',
                    isArray: true
                }
            })
        };

        api.contacts = {
            contacts: $resource(api.mongoUrl + '/contacts'),
            user: $resource(api.mongoUrl + '/users'),
            deleteContacts: $resource(api.mongoUrl + '/contacts/:id', { id: '@id' }),
            addContacts: $resource(api.mongoUrl + '/contacts', {}, {
                post: {
                    method: 'POST',
                    headers: { 'content-Type': 'Application/Json' }
                }
            }

            ),
            saveContact: $resource(api.mongoUrl + '/contacts/:id', { id: '@id' },
                {
                    put: {
                        method: 'PUT',
                        headers: { 'content-Type': 'Application/Json' }
                    }
                }
            )
        };

        api.calendar = {
            getAppointments: $resource(api.baseUrl + '/calendar-backend/api/v1/appointments/', {}),
            createAppointment: $resource(api.baseUrl + '/calendar-backend/api/v1/appointments/', {}, {
                post: {
                    method: 'POST',
                    isArray: false
                }
            }),
            deleteAppointments: $resource(api.baseUrl + '/calendar-backend/api/v1/appointments/:id', { id: '@id' }),
            getBranches: $resource(api.baseUrl + '/calendar-backend/api/v1/branches/', {}),
            getResources: $resource(api.baseUrl + '/calendar-backend/api/v1/resources/', {}),
            getServices: $resource(api.baseUrl + '/calendar-backend/api/v1/services/', {})
        };

        api.chat =
        {
            get:
            $resource
            (
                api.mongoUrl + '/contacts',
                {
                    get:
                    {
                        method: 'GET'
                    }
                }
            ),
            getById:
            $resource
            (
                api.mongoUrl + '/contacts/:id',
                {
                    id: '@id'
                },
                {
                    get:
                    {
                        method: 'GET'
                    }
                }
            ),
            save:
            $resource
            (
                api.mongoUrl + '/contacts/:id',
                {
                    id: '@id'
                },
                {
                    put:
                    {
                        method: 'PUT',
                        headers: { 'content-Type': 'Application/Json' }
                    }
                }
            )
        };

        api.user =
        {
            create:
            $resource
            (
                api.mongoUrl + '/users', {},
                {

                    post:
                    {
                        method: 'POST',
                        headers: { 'content-Type': 'Application/Json' }
                    }
                }
            ),
            getUsers:
            $resource
            (
                api.mongoUrl + '/users', {},
                {
                    get:
                    {
                        method: 'GET',
                        isArray: true
                    }
                }
            ),
            getById:
            $resource
            (
                api.mongoUrl + '/users/:id',
                {
                    id: '@id'
                },
                {
                    get:
                    {
                        method: 'GET'
                    }
                }
            ),
            save:
            $resource
            (
                api.mongoUrl + '/users/:id',
                {
                    id: '@id'
                },
                {
                    put:
                    {
                        method: 'PUT',
                        headers: { 'content-Type': 'Application/Json' }
                    }
                }
            )
       };

       api.config =
       {
           get:
           $resource
           (
               api.mongoUrl + '/config', {},
               {
                   get:
                   {
                       method: 'GET',
                       isArray: true
                   }
               }
           ),
            saveConfig:
                $resource(api.mongoUrl + '/config/:id',
                   {
                       id: '@id'
                   },
                   {
                       put:
                       {
                           method: 'PUT',
                           headers: { 'content-Type': 'Application/Json' }
                       }
                   }
                ),
            createConfig:
               $resource(api.mongoUrl + '/config', {},
                    {
                        post:
                        {
                            method: 'POST',
                            headers: { 'content-Type': 'Application/Json' },
                            isArray: false
                        }
                    }
                )
       };

        api.settings =
        {
            getSettings:
                $resource(api.mongoUrl + '/settings', {},
                    {
                        get:
                        {
                            method: 'GET',
                            isArray: false
                        }
                    }
                 ),

            saveSettings:
                $resource(api.mongoUrl + '/settings/:id',
                   {
                       id: '@id'
                   },
                   {
                       put:
                       {
                           method: 'PUT',
                           headers: { 'content-Type': 'Application/Json' }
                       }
                   }
                ),

           createSettings:
               $resource(api.mongoUrl + '/settings', {},
                    {
                        post:
                        {
                            method: 'POST',
                            headers: { 'content-Type': 'Application/Json' }
                        }
                    }
                )
        };

        api.questionaires =
        {
            getQuestionaires:
              $resource(api.mongoUrl + '/questionaire', {},
                  {
                      get:
                      {
                          method: 'GET',
                          isArray: false
                      }
                  }
               )
        };
        api.cfu =
        {
            createCFU:
                 $resource(api.mongoUrl + '/CFU', {},
                    {
                        post:
                        {
                            method: 'POST',
                            headers: { 'content-Type': 'Application/Json' }
                        }
                    }
                )
        };

        api.images =
        {
            getImages:
                $resource(api.mongoUrl + '/pictures', {},
                    {
                        get:
                        {
                            method: 'GET',
                            isArray: true
                        }
                    }
                 ),

            saveImages:
                $resource(api.mongoUrl + '/pictures/:id',
                   {
                       id: '@id'
                   },
                   {
                       put:
                       {
                           method: 'PUT',
                           headers: { 'content-Type': 'Application/Json' }
                       }
                   }
                ),

            createImages:
                $resource(api.mongoUrl + '/pictures', {},
                     {
                         post:
                         {
                             method: 'POST',
                             headers: { 'content-Type': 'Application/Json' }
                         }
                     }
                 )
        };

        api.queues =
       {
           getQueues:
           $resource
           (
               api.mongoUrl + '/queues', {},
               {
                   get:
                   {
                       method: 'GET',
                       isArray: true
                   }
               }
           ),
            getQueuesById:
            $resource
            (
                api.mongoUrl + '/queues/:id',
                {
                    id: '@id'
                },
                {
                    get:
                    {
                        method: 'GET'
                    }
                }
            ),
            saveQueues:
                $resource(api.mongoUrl + '/queues/:id',
                   {
                       id: '@id'
                   },
                   {
                       put:
                       {
                           method: 'PUT',
                           headers: { 'content-Type': 'Application/Json' }
                       }
                   }
                ),
            createQueues:
               $resource(api.mongoUrl + '/queues', {},
                    {
                        post:
                        {
                            method: 'POST',
                            headers: { 'content-Type': 'Application/Json' }                            
                        }
                    }
                )
       };
        
       api.services =
       {
           getServices:
           $resource
           (
               api.mongoUrl + '/Services', {},
               {
                   get:
                   {
                       method: 'GET',
                       isArray: true
                   }
               }
           ),
            saveServices:
                $resource(api.mongoUrl + '/Services/:id',
                    {
                        id: '@id'
                    },
                    {
                        put:
                        {
                            method: 'PUT',
                            headers: { 'content-Type': 'Application/Json' }
                        }
                    }
                ),
            createServices:
               $resource(api.mongoUrl + '/Services', {},
                {
                    post:
                    {
                        method: 'POST',
                        headers: { 'content-Type': 'Application/Json' }                            
                    }
                }
                )
       };

       api.queueMapping =
       {
           getQueueMapping:
           $resource
           (
               api.mongoUrl + '/queueMapping', {},
               {
                   get:
                   {
                       method: 'GET',
                       isArray: true
                   }
               }
           ),
            saveQueueMapping:
                $resource(api.mongoUrl + '/queueMapping/:id',
                   {
                       id: '@id'
                   },
                   {
                       put:
                       {
                           method: 'PUT',
                           headers: { 'content-Type': 'Application/Json' }
                       }
                   }
                ),
            createQueueMapping:
               $resource(api.mongoUrl + '/queueMapping', {},
                    {
                        post:
                        {
                            method: 'POST',
                            headers: { 'content-Type': 'Application/Json' }                            
                        }
                    }
                )
       };

       api.visits =
       {
           getVisits:
           $resource
           (
               api.mongoUrl + '/visit', {},
               {
                   get:
                   {
                       method: 'GET',
                       isArray: true
                   }
               }
           ),
            saveVisits:
                $resource(api.mongoUrl + '/visit/:id',
                   {
                       id: '@id'
                   },
                   {
                       put:
                       {
                           method: 'PUT',
                           headers: { 'content-Type': 'Application/Json' }
                       }
                   }
                ),
            createVisits:
               $resource(api.mongoUrl + '/visit', {},
                    {
                        post:
                        {
                            method: 'POST',
                            headers: { 'content-Type': 'Application/Json' }
                            
                        }
                    }
                ),
            deleteVisit:
                $resource(api.mongoUrl + '/visit/:id',
                { id: '@id' }
                ,
                {
                    delete:
                    {
                        method: 'DELETE',
                        headers: { 'content-Type': 'Application/Json' }
                    }
                }
            ),
       };

        return api;
    }   

})();