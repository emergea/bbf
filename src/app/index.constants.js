(function ()
{
    'use strict';

    angular
        .module('fuse')
        .constant('AUTH_EVENTS',
        {
            notAuthenticated: 'auth-not-authenticated'
        },
        'API_ENDPOINT',
        {
           //TODO: This needs to change to extract the base url form the database.
           url: 'http://localhost:4000/'
        })
})();
