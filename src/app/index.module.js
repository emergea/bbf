(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Core
            'app.core',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick Panel
            'app.quick-panel',

            // Sample
            //'app.sample',
            'app.mobile',
            'app.chat',
	        'app.calendar',
            'app.contacts',

            'app.settings',
            'app.survey',
            'app.staffRating',

            //Auth
            'app.pages.auth.lock',
            'app.pages.auth.login',
            'app.pages.auth.register',
            'app.pages.auth.reset-password',

            //Profile
            'app.pages.profile',

            //Queuing
            'app.linearQueueing',

            //Dashboard
            'app.dashboard'

        ]);
})();