(function () {
    'use strict';

    angular
        .module('app.core')
        .service('AllImages', AllImages);

        function AllImages(api) {

            var vm = this;
            vm.getImages = getImages;
            vm.images;
            vm.image;
            vm.loadedImages = loadedImages;



            function getImages() {
                //API get branches
                api.images.getImages.query
                (
                    //Success response
                    function (response) {
                        vm.images = response;
                    },

                    //Failure response
                    function (response) {
                        console.error(response);
                    }
                );
            };

            function loadedImages() {
                return vm.images;
            }

            return vm;
        }

})();