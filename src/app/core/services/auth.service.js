(function ()
{
    'use strict';

    angular
        .module('app.core')
        .service('AuthService', function($q, $http, $mdDialog, $state, $resource,$rootScope)
        {
            var LOCAL_TOKEN_KEY = 'yourTokenKey';
            var isAuthenticated = false;
            var authToken;
            
            var vm = this; 
            vm.login = login;
            vm.register = register;
            vm.logout = logout;
            vm.API_ENDPOINT = '';
            vm.isAuthenticated = false;
            vm.authToken;
            vm.user;
            vm.baseUrl;
            vm.mongoUrl;


        $rootScope.$on('set-baseUrl', function (event, args) {
            vm.baseUrl = args.baseUrl;
            console.log(args.baseUrl);
        });

        $rootScope.$on('set-mongoUrl', function (event, args) {
            vm.mongoUrl = args.mongoUrl;
            vm.API_ENDPOINT = vm.mongoUrl
            console.log(args.mongoUrl);
        });


    
            /** @ngInject */
            function loadUserCredentials()
            {
                var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
                if (token) {
                useCredentials(token);
                }
            }
    
            function storeUserCredentials(token)
            {
                window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
                useCredentials(token);
            }
    
            function useCredentials(token)
            {
                vm.isAuthenticated = true;
                vm.authToken = token;
            
                // Set the token as header for your requests!
                $http.defaults.headers.common.Authorization = vm.authToken;
            }
    
            function destroyUserCredentials()
            {
                vm.authToken = undefined;
                vm.isAuthenticated = false;
                $http.defaults.headers.common.Authorization = undefined;
                window.localStorage.removeItem(LOCAL_TOKEN_KEY);
            }
    
            function register(user)
            {
                return $q(function(resolve, reject)
                {
                    $http.post(vm.API_ENDPOINT + '/signup', user).then(function(result)
                    {
                        if (result.data.success)
                        {                            
                            resolve(result.data.msg);
                        }
                        else
                        {                            
                            reject(result.data.msg);
                        }
                    });
                });
            };
    
            function login(user)
            {
                return $q(function(resolve, reject)
                {
                    $http.post(vm.API_ENDPOINT + '/users/authenticate', user).then(function(result)
                    {            
                        if (result.data.success)
                        {                            
                            storeUserCredentials(result.data.token);
                            resolve(result.data.msg);
                            vm.user = user;
                            $state.go('app.mobile');
                        } 
                        else
                        {
                            ShowAlert('User Login Failure',result.data.msg, 'User Login Failure', 'ok');
                            reject(result.data.msg);
                        }
                    });
                });
            };

            function user(user) {

                vm.user = user;
            };
    
            function logout()
            {
                destroyUserCredentials();
            };
    
            loadUserCredentials();

            //TODO: This should form part of the app as a whole, i.e should be accessable by all controllers to display errors.
            function ShowAlert (title, textContent, ariaLabel, ok)
            {            
                // Modal dialogs should fully cover application
                // to prevent interaction outside of dialog
                $mdDialog.show
                (
                    $mdDialog.alert()                
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(textContent)
                    .ariaLabel(ariaLabel)
                    .ok(ok)
                );
            }
    
            return
            {
                login: login
                register: register
                logout: logout
                isAuthenticated: {return vm.isAuthenticated;}
            };
    })

    
    
    .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
        $rootScope.$broadcast({
            401: AUTH_EVENTS.notAuthenticated,
        }[response.status], response);
        return $q.reject(response);
        }
    };
    })
    
    .config(function ($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
    });
})();