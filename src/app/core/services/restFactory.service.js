(function ()
{
    'use strict';

    angular
        .module('app.core')
        .service('restFactory', function($q, $http, $resource)
        {
            var mountingPoint = '/rest';
            
            return {
            getUser: function () {
                return $http(
                    {
                        method: "GET",
                        url: mountingPoint + "/servicepoint/user",
                        cache: false
                    });
            },
            getGlobalParameters: function () {
                return $http(
                    {
                        method: "GET",
                        url: "/rest/servicepoint/variables",
                        cache: false
                    });
            },
            getGlobalParameter: function (parameter) {
                return $http(
                    {
                        method: "GET",
                        url: "/rest/servicepoint/variables/" + parameter,
                        cache: false,
                        async: false
                    });
            },
            deleteGlobalParameter: function (parameter) {
                return $http(
                    {
                        method: "DELETE",
                        url: "/rest/servicepoint/variables/" + parameter,
                        cache: false
                    });
            },
            addGlobalParameter: function (addMappingObject) {
                return $http(
                    {
                        method: "PUT",
                        url: "rest/servicepoint/variables/",
                        data: {
                            name: addMappingObject.name,
                            value: addMappingObject.value
                        }
                    });
            },
            getBranches: function () {
                return $http(
                    {
                        method: "GET",
                        url: mountingPoint + "/managementinformation/v2/branches",
                        cache: false
                    });
            },
            getServicePoints: function (branchId) {
                return $http(
                    {
                        method: "GET",
                        url: mountingPoint + "/managementinformation/v2/branches/" + branchId + "/servicePoints",
                        cache: false
                    });
            },
            getQueues: function (branchId) {
                return $http(
                    {
                        method: "GET",
                        url: mountingPoint + "/managementinformation/v2/branches/" + branchId + "/queues",
                        cache: false
                    });
            },
            getVisits: function (branchId, queueId) {
                return $http(
                    {
                        method: "GET",
                        url: mountingPoint + "/managementinformation/v2/branches/" + branchId + "/queues/" + queueId + "/visits",
                        cache: false
                    });
            },
            getAvgTransactionTime: function () {
                return $http(
                    {
                        method: "GET",
                        url: "/EQWebservices/GetAverageTransactionTime",
                        cache: false
                    });
            },
            getAvgWaitingTime: function () {
                return $http(
                    {
                        method: "GET",
                        url: "/EQWebservices/GetAverageWaitingTime",
                        cache: false,
                        async: false
                    });
            },
            getWaitingTimePerBranch: function (branchId) {
                return $http(
                    {
                        method: "GET",
                        url: "/EQWebservices/GetWaitingTimePerBranch?branchId=" + branchId,
                        cache: false,
                        async: false
                    });
            }
            }
        });
})();