﻿(function ()
{
    'use strict';

    angular
        .module('app.survey')
        .controller('SurveyController', SurveyController);

    function SurveyController(QuestionaireData, $state, SettingsData, api, $location, $rootScope) {
       
        var vm = this;

        //Data
        vm.surveys = QuestionaireData;
        vm.settings = SettingsData;
        vm.selectedSurvey = {};
        vm.currentPage = 'home';
        vm.pageNum = 1;
        vm.prevPage = 0;
        vm.lastPage = 0;
        vm.questionNumber = 0;
        vm.pageChange = pageChange;
        vm.selectedAnswer = selectedAnswer;
        vm.answers = '';
        vm.commitAnswers = commitAnswers;
        vm.radiobtn = '';
        vm.listItem = '';
        vm.textArea = '';
        vm.dropDown = '';
        vm.survey = {};
        vm.exit = exit;
        vm.layoutOpt = '';
        vm.answer = '';

        //Load saved load option, default survey and staff rating setting
        if (vm.settings.length > 0) {
            angular.forEach(vm.surveys, function (value) {
                if (value._id == vm.settings[0].cfuSettings.defaultSurvey) {
                    vm.selectedSurvey = value;
                    vm.survey = {
                        "name": value.name,
                        "branchId": null,
                        "orchestraTicketNumber": '',
                        "note": '',
                        "created_at": new Date(),
                        "answers": [],
                        "questions": value.questions,
                        "orchestravisitId": []
                    };
                    vm.layoutOpt = 'wizzard';
                    vm.lastPage = value.questions.length + 2;

                    if ($rootScope.staffRating) {
                        vm.currentPage = vm.layoutOpt;
                        pageChange(vm.layoutOpt);
                    }
                      
                    return
                }
            });
        };

        function pageChange(value) {

            if (vm.currentPage == 'home') {
                if (vm.settings[0].cfuSettings.staffRating) {
                    $location.path('/staffRating');
                };
            } else {
                vm.prevPage = vm.prevPage + 1;
                vm.pageNum = vm.pageNum + 1;
            };

            if (vm.pageNum != vm.lastPage) {
                if (vm.layoutOpt == 'wizzard') {
                    vm.currentPage = 'questions';
                } else {
                    vm.currentPage = 'one-page';
                };
            } else {
                vm.currentPage = value;
            };

            if (vm.prevPage > 1) {
                vm.questionNumber = vm.questionNumber + 1;
            }
        };

        function selectedAnswer(questionNum) {

            if (vm.radiobtn != '') {
                vm.answers = vm.answers + ',' + vm.radiobtn;
                vm.survey.questions[vm.questionNumber].Answer = vm.radiobtn;
                vm.radiobtn = '';
            };

            if (vm.listItem != '') {
                vm.answers = vm.answers + ',' + vm.listItem;
                vm.survey.questions[vm.questionNumber].Answer = vm.listItem;
                vm.listItem = '';
            };

            if (vm.textArea != '') {
                vm.answers = vm.answers + ',' + vm.textArea;
                vm.survey.questions[vm.questionNumber].Answer = vm.textArea;
                vm.textArea = '';
            };

            if (vm.dropDown != '') {
                vm.answers = vm.answers + ',' + vm.dropDown;
                vm.survey.questions[vm.questionNumber].Answer = vm.dropDown;
                vm.dropDown = '';
            };
                  
            if (vm.lastPage == vm.pageNum + 1) {
                pageChange('last');
            } else if (vm.currentPage == 'one-page') {
                vm.pageNum = vm.lastPage - 1;
                pageChange('last');
            } else {
                pageChange('questions');
            };

            
        };

        function commitAnswers() {
            vm.survey.created_at = new Date();
            vm.answers = vm.answers.substring(1);
            vm.survey.answers = [vm.answers];
            vm.answers = '';
            vm.survey.staffRating = $rootScope.rating;
            api.cfu.createCFU.post(vm.survey,

                // Success
                function (response) {
                    console.log(response);

                },

                // Error
                function (response) {
                    console.error(response);
                }
            );

            vm.currentPage = 'home';
            vm.pageNum = 1;
            vm.prevPage = 0;
            vm.questionNumber = 0;
            vm.survey = {};
        };

        function exit() {
            $location.path('/mobile');
        };

        vm.getImage = function (data) {
            return 'data:image/jpeg;base64,' + data;
        };

        vm.layout = 'onePage';
        
        console.log(vm.selectedSurvey);
    }            
})();