﻿(function () {
    'use strict';

    angular
    .module('app.survey', [])
    .config(config);

    function config($stateProvider, msApiProvider, msNavigationServiceProvider) {
        $stateProvider
                .state('app.survey', {
                    url: '/survey',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/survey/survey.html',
                            controller: 'SurveyController as vm'
                        }
                    },
                    resolve: {
                        QuestionaireData: function (apiResolver) {
                            return apiResolver.resolve('questionaires.getQuestionaires@query');
                        },
                        SettingsData: function (apiResolver) {
                            return apiResolver.resolve('settings.getSettings@query');
                        }
                    }
                })

        // Api
        //msApiProvider.register('mobile', ['app/data/qmatic/data.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.survey', {
            title: 'Survey',
            icon: 'icon-tile-four',
            state: 'app.survey',
            weight: 1
        });
    }
})();