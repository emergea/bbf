﻿(function () {
    'use strict';

    angular
    .module('app.mobile', ['swipe'])
    .config(config);

    function config($stateProvider, msApiProvider, msNavigationServiceProvider)
    {
        $stateProvider
                .state('app.mobile', {
                    url: '/mobile',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/mobile.html',
                            controller: 'MobileController as vm'
                        }
                    },
                    resolve: {
                        MobileData: function (msApi) 
                        {
                                return msApi.resolve('mobile@get');
                        },
                        Configs: function (apiResolver)
                        {
                            return apiResolver.resolve('config.get@query');
                        }
                    }
                })

               .state(
                    'app.mobile.orchestra', {
                        url: '/orchestra',
                        views: {
                            'content@app': {
                                templateUrl: 'app/main/mobilecommandcenter/orchestra/orchestra.html',
                                controller: 'OrchestraController as vm'
                            }
                        },
                        resolve: {
                            MobileData: function (msApi) {
                                return msApi.resolve('mobile@get');
                            }
                        }
                    })

                .state(
                    'app.mobile.cfu', {
                        url: '/cfu',
                        views: {
                            'content@app': {
                                templateUrl: 'app/main/mobilecommandcenter/cfu/cfu.html',
                                controller: 'CFUController as vm'
                            }
                        },
                        resolve: {
                            QuestionData: function (msApi) {
                                return msApi.resolve('cfu@get');
                            }
                        }
                    })

                //In order for step4 to know about the app.mobile.games state it needs to be declared in this module.
                //It can not therefore have its own module... Further understanding needed.
                //Do not comment out, or you will not reach the games page.
                .state(
                   'app.mobile.games', {
                       url: '/games?stepId=:stepIdParam&branchId=:branchIdParam',
                       views: {
                           'content@app': {
                               templateUrl: 'app/main/mobilecommandcenter/games/games.html',
                               controller: 'GamesController as vm'
                           }
                       },
                       resolve: {
                           GamesData: function (msApi) {
                               if (operationsMode == "orchestra") {
                                   return msApi.resolve('game@get');
                               }
                           }
                       }
                   })
                   
        msApiProvider.register('mobile', ['app/data/qmatic/data.json']);
        msApiProvider.register('cfu', ['app/data/cfu/questions.json']);
        msApiProvider.register('games', ['app/data/games/game.json']);        

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.mobile', {
            title: 'Mobile',
            icon: 'icon-tile-four',
            state: 'app.mobile',
            weight: 1
        });
    }
})();