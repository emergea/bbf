(function ()
{
    'use strict';

    angular
        .module('app.pages.profile')
        .controller('ProfileController', ProfileController);

    /** @ngInject */
    function ProfileController(About, AuthService, $filter, $scope, api, $mdToast, AllImages) {
        var vm = this;
        vm.user;
        vm.about;
        vm.updateUser = updateUser;
        vm.updateImage = updateImage;
        vm.images;
        vm.image;
        vm.imageId;
        vm.fileName;
        vm.createImages = createImages;

        api.images.getImages.query(

            // Success
            function (response) {
                vm.images = response
            },

            // Error
            function (response) {
                console.error(response);
            }
        );

        // Data
        vm.users = About;

        //Get active user
        if (AuthService.user != undefined) {

            vm.user = $filter('filter')(vm.users, function (item) {
                return item.email == AuthService.user.email;
            });

            vm.about = vm.user[0]
        };

        // Methods

        function updateImage() {
            if (vm.about != undefined) {
                vm.image = $filter('filter')(vm.images, function (item) {
                    return item.img == vm.about.avatar;
                });

                if (vm.image == undefined || vm.image.length == 0) {
                    vm.image = { "img": vm.about.avatar, "name": vm.fileName }

                    createImages(vm.image);
                };
            }
        };

        function createImages(image) {
            //Add images to db
            api.images.createImages.post(image,

                    // Success
                    function (response) {
                        vm.about.avatar = response;
                    },

                    // Error
                    function (response) {
                        console.error(response);
                    }
                );
        };

        function updateUser() {
        //    //Add contact to db
        //    api.user.save.put({ 'id': vm.about._id }, vm.about,

        //        // Success
        //        function (response) {
        //            var message = 'Profile Updated';
        //            $mdToast.show({
        //                template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
        //                hideDelay: 7000,
        //                position: 'top right',
        //                parent: '#content'
        //            });

        //        },

        //        // Error
        //        function (response) {
        //            console.error(response);
        //        }
        //    );
        };

        $scope.stepsModel = [];

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                vm.about.avatar = e.target.result;
            });
        }

        $scope.imageUpload = function (element) {
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(element.target.files[0]);
            vm.fileName = element.target.files[0].name;
        }

        //////////
    }

})();
