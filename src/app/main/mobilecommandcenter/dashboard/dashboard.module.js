﻿(function () {
    'use strict';

    angular
    .module('app.dashboard',
            [
                // 3rd Party Dependencies
                'nvd3'
            ]
    )
    .config(config);

    function config($stateProvider, msApiProvider, msNavigationServiceProvider, $translatePartialLoaderProvider) {
        $stateProvider
                .state('app.dashboard', {
                    url: '/dashboard',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/dashboard/dashboard.html',
                            controller: 'DashboardController as vm'
                        }
                    },
                    resolve: {
                        MobileData: function (msApi) 
                        {
                            return msApi.resolve('mobile@get');
                        },
                        TownAndProvinceData: function (msApi)
                        {
                            return msApi.resolve('dashboard.TownAndProvince@get');
                        }
                    }
                })

        // Api
        msApiProvider.register('mobile', ['app/data/qmatic/data.json']);

        // Api
        msApiProvider.register('dashboard.TownAndProvince', ['app/data/dashboard/TownAndProvince/data.json']);

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/mobilecommandcenter/dashboard');

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.dashboard', {
            title: 'Dashboard',
            icon: 'icon-chart-areaspline',
            state: 'app.dashboard',
            weight: 1
        });
    }
})();