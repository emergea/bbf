﻿(function ()
{
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardController', DashboardController);

    function DashboardController(MobileData, $state, $scope, restFactory, $q, TownAndProvinceData)
    {
       
        var vm = this;
        
        //Data        
        vm.visitsPromiseOrder = [];
        vm.retrieveDataInterval = null;
        vm.transactionTimeRetrieveInterval = null;
        vm.queuesDone = false;
        vm.servicePointsDone = false;
        vm.visitsDone = false
        vm.pieData;
        vm.log = false; // Will produce allot of output in prod, use only for dev/prod issues.

        vm.col1 = '';
        vm.col2 = '';
        vm.col2end = '';
        vm.col3 = '';

        vm.refreshDate = new Date();
        vm.showOpsPanel = false;
        vm.serviceLevel = null;

        vm.userSettings = 
        {
            username: "superadmin",
            password: "ulan"
        }

        vm.nationalData = 
        {
            "customersWaiting": 0,
            "visits": 0,
            "waitingMoreThan5": 0,
            "openCounters": 0,
            "servingCounters": 0,
            "avgTransactionTimeSeconds": 0,
            "totalWaitingTime": 0,
            "historicAvgWaitingTime": 0,
            "serviceLevels":
            {
                "lowerBracket": 0, // less than or equal to service level
                "middleBracket": 0, // more than service level, less than or equal to (service level + 10)
                "upperBracket": 0, // more than (service level + 10)
                "historicLowerBracket": 0, // less than or equal to service level
                "historicMiddleBracket": 0, // more than service level, less than or equal to (service level + 10)
                "historicUpperBracket": 0 // more than (service level + 10)
            }
        };

        vm.townAndProvinceData = TownAndProvinceData.TownAndProvince;
        console.log(vm.townAndProvinceData);
        
        vm.branchesData = [];
        vm.brackets = [];

        vm.avgTt = 
        {
            value1: 0,
            value2: 0
        };

        vm.avgWt = 
        {
            value1: 0,
            value2: 0
        }

        vm.slInt =
        {
            value1: 0,
            value2: 0
        }

        vm.avgWaitingTimeClassIntervals =
        {
            value1: 5,
            value2: 10
        };

        vm.avgTransactionTimeClassIntervals =
        {
            value1: 10,
            value2: 15
        };

        vm.serviceLevelClassIntervals =
        {
            value1: 95,
            value2: 90
        }

        vm.settingsErr = '';

        vm.colors =
        {
            color1: '',
            color2: '',
            color3: '',
            color4: '',
            colorsSet: false
        };

        vm.zero = 0;
        vm.greenRule;
        vm.yellowRule;
        vm.redRule;

        //Methods
        vm.collapseIt = collapseIt;
        vm.secondsToTime = secondsToTime;
        vm.calculateAvgWaitingTime = calculateAvgWaitingTime ;
        vm.DetermineServiceLevelValue = DetermineServiceLevelValue;
        vm.DetermineServiceLevelClass = DetermineServiceLevelClass;
        vm.DetermineAvgWaitingTimeClass = DetermineAvgWaitingTimeClass;
        vm.DetermineAvgTransactionTimeClass = DetermineAvgTransactionTimeClass;
        vm.GetDateTime = GetDateTime;
        vm.getRule = getRule;
        vm.applyColorRules = applyColorRules;
        vm.initColorPickers = initColorPickers;
        vm.retrieveIntervalSettings = retrieveIntervalSettings;
        vm.retrieveAllData = retrieveAllData;
        vm.checkAndStartRetrieveTimer = checkAndStartRetrieveTimer;
        vm.setBrackets = setBrackets;
        vm.retrieveAvgTransactionTime  = retrieveAvgTransactionTime;
        vm.retrieveServicePoints = retrieveServicePoints;
        vm.retrieveQueues = retrieveQueues;
        vm.retrieveVisits = retrieveVisits;
        vm.processServicePoints = processServicePoints;
        vm.processQueues = processQueues;
        vm.drawPieChart = drawPieChart;
        vm.setExpanded = setExpanded;

        //Embedded StyleSheets
        //TODO: Need to be able to set these range from a config.
        vm.goodTime = 5;
        vm.goodStyle = {'background-color':'#BAE395'};
        vm.mediumTime = 10;        
        vm.mediumStyle = {'background-color':'#374867'};
        vm.badTime = 15;
        vm.badStyle = {'background-color':'#FF3C65'};
        //We are using this to apply to the ng-style, because the division is with div's;
        // and we need to change the background color, as wel as give the border style and keep the two separate.
        vm.borderStyle = {'border':'0px solid grey'};


        vm.dashboardData = 
        {
            "widget6": 
                {
                "title": "",
                "ranges":
                {
                    "TW": "This Week",
                    "LW": "Last Week",
                    "2W": "2 Weeks Ago"
                },
                "mainChart":
                [
                    {
                        "label": vm.goodTime + " min",
                        "values": {
                            "2W": 0,
                            "LW": 0,
                            "TW": 15
                        }
                    },
                    {
                        "label": vm.goodTime + " - " + vm.mediumTime + " min",
                        "values":
                        {
                            "2W": 0,
                            "LW": 0,
                            "TW": 30
                        }
                    },
                    {
                        "label": vm.badTime + " min plus",
                        "values":
                        {
                            "2W": 0,
                            "LW": 0,
                            "TW": 55
                        }
                    }
                ],
                "footerLeft": {
                    "title": "Visits Created",
                    "count": {
                        "2W": -1,
                        "LW": -1,
                        "TW": -1
                    }
                },
                "footerRight": {
                    "title": "Visits Completed",
                    "count": {
                        "2W": -1,
                        "LW": -1,
                        "TW": -1
                    }
                }
            }
        }

        vm.tableTemplate =
        {
            
            ProvinceData: 
            [
                {
                    "Region": "Eastern Cape",
                    "RegionData":                    
                    {                    
                        "branchName": "All Branches",
                        "openCounters": 0,
                        "servingCounters": 0,                
                        "waitTimeMoreThan5": 0,
                        "customersWaiting": 0,
                        "expanded": false
                    },
                    "BranchData": []
                },
                {
                    "Region": "Johannesburg",
                    "RegionData":
                    {                    
                        "branchName": "All Branches",
                        "openCounters": 0,
                        "servingCounters": 0,                
                        "waitTimeMoreThan5": 0,
                        "customersWaiting": 0,
                        "expanded": false
                    },
                    "BranchData": []
                },
                {
                    "Region": "Western Cape",
                    "RegionData":
                    {                    
                        "branchName": "All Branches",
                        "openCounters": 0,
                        "servingCounters": 0,                
                        "waitTimeMoreThan5": 0,
                        "customersWaiting": 0,
                        "expanded": false
                    },
                    "BranchData": [] 
                }
            ]
            
        }

        // Widget 6
        vm.widget6 = {
            title       : vm.dashboardData.widget6.title,
            mainChart   : {
                config : {
                    refreshDataOnly: true,
                    deepWatchData  : true
                },
                options: {
                    chart: {
                        type        : 'pieChart',
                        color       : ['#BAE395', '#374867', '#FF3C65'],
                        height      : 400,
                        margin      : {
                            top   : 0,
                            right : 0,
                            bottom: 0,
                            left  : 0
                        },
                        donut       : true,
                        clipEdge    : true,
                        cornerRadius: 0,
                        labelType   : 'percent',
                        padAngle    : 0.02,
                        x           : function (d)
                        {
                            return d.label;
                        },
                        y           : function (d)
                        {
                            return d.value;
                        },
                        tooltip     : {
                            gravity: 's',
                            classes: 'gravity-s'
                        }
                    }
                },
                data   : []
            },
            // footerLeft  : vm.dashboardData.widget6.footerLeft,
            // footerRight : vm.dashboardData.widget6.footerRight,
            // ranges      : vm.dashboardData.widget6.ranges,
            currentRange: '',
            changeRange : function (range)
            {
                vm.widget6.currentRange = range;

                /**
                 * Update main chart data by iterating through the
                 * chart dataset and separately adding every single
                 * dataset by hand.
                 *
                 * You MUST NOT swap the entire data object by doing
                 * something similar to this:
                 * vm.widget.mainChart.data = chartData
                 *
                 * It would be easier but it won't work with the
                 * live updating / animated charts due to how d3
                 * works.
                 *
                 * If you don't need animated / live updating charts,
                 * you can simplify these greatly.
                 */
                angular.forEach(vm.dashboardData.widget6.mainChart, function (data, index)
                {
                    var rangeValue;                    

                    if(data.label == vm.goodTime + " min")
                    {
                        rangeValue = vm.pieData[index].value;
                    }                    
                    else if(vm.goodTime + " - " + vm.mediumTime + " min")
                    {
                        rangeValue = vm.pieData[index].value;
                    }
                    else if(vm.goodTime + " - " + vm.mediumTime + " min")
                    {
                        rangeValue = vm.pieData[index].value;
                    }                    

                    vm.widget6.mainChart.data[index] = 
                    {
                        label: data.label,
                        value: rangeValue
                        //value: data.values[range]
                    };

                });
            },
            init        : function ()
            {
                // Run this function once to initialize widget

                /**
                 * Update the range for the first time
                 */
                vm.widget6.changeRange('TW');
            }
        };
        
        /*
         * collapseIt
         */
        function collapseIt(id)
        {
            $scope.collapseId = ($scope.collapseId==id)?-1:id;
        }

        function secondsToTime(secs)
        {
             if (secs != undefined && secs > 0)
            {
                var hours = Math.floor(secs / (60 * 60));

                var divisor_for_minutes = secs % (60 * 60);
                var minutes = Math.floor(divisor_for_minutes / 60);

                var divisor_for_seconds = divisor_for_minutes % 60;
                var seconds = Math.ceil(divisor_for_seconds);

                var time = PadLeft(hours) + ":" + PadLeft(minutes);
                return time;
            }
            else
            {
                return "00:00";
            }
        };

        function calculateAvgWaitingTime(totalWaitingTime, waitingCustomers) 
        {
            if (totalWaitingTime != null && waitingCustomers != null)
            {
                if (totalWaitingTime > 0 && waitingCustomers > 0)
                {
                    var avg = totalWaitingTime / waitingCustomers;

                    if (vm.nationalData.historicAvgWaitingTime != null && vm.nationalData.historicAvgWaitingTime > 0) {
                        if (avg > 0)
                        {
                            return (avg + vm.nationalData.historicAvgWaitingTime) / 2;
                        }
                        else
                        {
                            return vm.nationalData.historicAvgWaitingTime;
                        }
                    }
                    else
                    {
                        return avg;
                    }
                }
                else
                {
                    return vm.nationalData.historicAvgWaitingTime;
                }
            }
            else
            {
                return '';
            }
        };
    
        function DetermineServiceLevelValue(serviceLevels)
        {
            if (serviceLevels != null)
            {
                var value = 0;
                var total = serviceLevels.lowerBracket + serviceLevels.middleBracket + serviceLevels.upperBracket +
                    serviceLevels.historicLowerBracket + serviceLevels.historicMiddleBracket + serviceLevels.historicUpperBracket;

                if ((serviceLevels.lowerBracket + serviceLevels.historicLowerBracket) > 0)
                {
                    value = serviceLevels.lowerBracket + serviceLevels.historicLowerBracket;
                }
                //if ((serviceLevels.middleBracket + serviceLevels.historicMiddleBracket) > 0) {
                //    value = serviceLevels.middleBracket + serviceLevels.historicMiddleBracket;
                //}
                //if ((serviceLevels.upperBracket + serviceLevels.historicUpperBracket) > 0) {
                //    value = serviceLevels.upperBracket + serviceLevels.historicUpperBracket;
                //}

                if (total > 0)
                {
                    return Math.round((value / total) * 100);
                }
                else
                {
                    return '-';
                }
            }
        }

        function DetermineServiceLevelClass(serviceLevels)
        {
            if (serviceLevels != null) 
            {
                var panelClass = 'panel-default';

                var percentage = vm.DetermineServiceLevelValue(serviceLevels);

                if ($.isNumeric(percentage))
                {
                    if (percentage >= serviceLevelClassIntervals.value2)
                    {
                        panelClass = 'panel-green';
                    }
                    else if (percentage < serviceLevelClassIntervals.value2 && percentage >= serviceLevelClassIntervals.value1)
                    {
                        panelClass = 'panel-yellow';
                    }
                    else
                    {
                        panelClass = 'panel-red';
                    }
                } 
                else
                {
                    panelClass = 'panel-default';
                }

                return panelClass;
            }
        }

        function DetermineAvgWaitingTimeClass(totalWaitingTime, waitingCustomers)
        {
            var avg = vm.calculateAvgWaitingTime(totalWaitingTime, waitingCustomers);

            if (avg != null) 
            {
                if (avg >= 0 && avg <= (avgWaitingTimeClassIntervals.value1 * 60))
                {
                    return 'panel-green';
                }
                else if (avg > (avgWaitingTimeClassIntervals.value1 * 60) && avg < (avgWaitingTimeClassIntervals.value2 * 60))
                {
                    return 'panel-yellow';
                }
                else
                {
                    return 'panel-red';
                }
            }
            else
            {
                return 'panel-green';
            }
        }

        function DetermineAvgTransactionTimeClass(avg)
         {

            if (avg != null && $.isNumeric(avg))
            {
                if (avg >= 0 && avg <= avgTransactionTimeClassIntervals.value1)
                {
                    return 'panel-green';
                }
                else if (avg > avgTransactionTimeClassIntervals.value1 && avg < avgTransactionTimeClassIntervals.value2)
                {
                    return 'panel-yellow';
                } else
                {
                    console.log('red: ' + avg);
                    return 'panel-red';
                }
            } 
            else 
            {
                return 'panel-green';
            }
        }
        
        function PadLeft(number)
        {
            if (number !== null && number !== undefined)
            {
                var str = "" + number;
                while (str.length < 2) str = "0" + str;
                return str;
            }
        }

        function GetDateTime()
        {
            var date = new Date();
            var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

            return str;
        }

        function getRule()
        {
            var rule;
            var ss = document.styleSheets;

            for (var i = 0; i < ss.length; ++i)
            {
                for (var x = 0; x < ss[i].cssRules.length; ++x)
                {
                    rule = ss[i].cssRules[x];

                    if (rule.name == "pulse" && rule.type == CSSRule.KEYFRAMES_RULE)
                    {
                        yellowRule = rule;
                    }
                    if (rule.selectorText == '.panel-green .panel-heading')
                    {
                        greenRule = rule;
                    }
                    if (rule.selectorText == '.panel-red .panel-heading')
                    {
                        redRule = rule;
                    }
                }
            }
        }
        
        function applyColorRules()
        {

            if (vm.colors.colorsSet == false)
            {
                //TODO: Rework ajax call for mongo.
                $.ajax({
                    url: '/rest/servicepoint/variables/nationalOpsPanelColors',
                    headers: {
                        'Authorization': 'Basic ' + btoa(vm.userSettings.username + ':' + vm.userSettings.password)
                    },
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data != null) {
                            var colorsArr = data.value.split('&');

                            if (colorsArr != null && colorsArr.length == 4) {

                                vm.colors.color1 = colorsArr[0];
                                vm.colors.color2 = colorsArr[1];
                                vm.colors.color3 = colorsArr[2];
                                vm.colors.color4 = colorsArr[3];
                                vm.colors.colorsSet = true;

                                vm.col1 = colorsArr[0];
                                vm.col2 = colorsArr[1];
                                vm.col2end = colorsArr[2];
                                vm.col3 = colorsArr[3];

                                initColorPickers();

                                getRule();

                                greenRule.style.backgroundColor = vm.colors.color1;
                                redRule.style.backgroundColor = vm.colors.color4;

                                yellowRule.deleteRule("0");
                                yellowRule.deleteRule("1");
                                yellowRule.appendRule("0% {background-color:" + vm.colors.color2 + ";}");
                                yellowRule.appendRule("100% {background-color: " + vm.colors.color3 + ";}");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(textStatus);
                        initColorPickers();
                    }
                });
            }
            else
            {

            }
        }
        
        function initColorPickers()
        {

            if (vm.colors.colorsSet) 
            {
                $('#c1').val(vm.col1);
                $('#c2').val(vm.col2);
                $('#c2end').val(vm.col2end);
                $('#c3').val(vm.col3);
            }

            $('.col1Class').colorpicker({ format: "hex" });
            $('.col2Class').colorpicker({ format: "hex" });
            $('.col2endClass').colorpicker({ format: "hex" });
            $('.col3Class').colorpicker({ format: "hex" });

            $('.col1Class').colorpicker().on('changeColor.colorpicker', function (event) 
            {
                vm.col1 = event.color.toHex();
            });

            $('.col2Class').colorpicker().on('changeColor.colorpicker', function (event)
            {
                vm.col2 = event.color.toHex();
            });

            $('.col2endClass').colorpicker().on('changeColor.colorpicker', function (event)
            {
                vm.col2end = event.color.toHex();
            });

            $('.col3Class').colorpicker().on('changeColor.colorpicker', function (event)
             {
                vm.col3 = event.color.toHex();
            });
        }

        function retrieveIntervalSettings()
        {
            //TODO Change the ajax calls to be mongo.
            $.ajax({
                url: '/rest/servicepoint/variables/nationalOpsPanelAvgWaitingTimeIntervals',
                headers: {
                    'Authorization': 'Basic ' + btoa(vm.userSettings.username + ':' + vm.userSettings.password)
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        var settings = data.value.split("&");

                        if (settings != null && settings.length == 2) {
                            vm.avgWt.value1 = settings[0];
                            vm.avgWt.value2 = settings[1];

                            avgWaitingTimeClassIntervals.value1 = settings[0];
                            avgWaitingTimeClassIntervals.value2 = settings[1];
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(textStatus);
                }
            });

            $.ajax({
                url: '/rest/servicepoint/variables/nationalOpsPanelAvgTransactionTimeIntervals',
                headers: {
                    'Authorization': 'Basic ' + btoa(vm.userSettings.username + ':' + vm.userSettings.password)
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        var settings = data.value.split("&");

                        if (settings != null && settings.length == 2) {
                            vm.avgTt.value1 = settings[0];
                            vm.avgTt.value2 = settings[1];

                            avgTransactionTimeClassIntervals.value1 = settings[0];
                            avgTransactionTimeClassIntervals.value2 = settings[1];
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(textStatus);
                }
            });

            $.ajax({
                url: '/rest/servicepoint/variables/nationalOpsPanelServiceLevelIntervals',
                headers: {
                    'Authorization': 'Basic ' + btoa(vm.userSettings.username + ':' + vm.userSettings.password)
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        var settings = data.value.split("&");

                        if (settings != null && settings.length == 2) {
                            vm.slInt.value1 = settings[0];
                            vm.slInt.value2 = settings[1];

                            serviceLevelClassIntervals.value1 = settings[0];
                            serviceLevelClassIntervals.value2 = settings[1];
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(textStatus);
                }
            });
        }

        function retrieveAllData(isRefresh)
        {

            if (vm.log)
            console.log(GetDateTime() + " - retrieveAllData called");

            //stop the timer if active
            if (vm.retrieveDataInterval != null)
            {
                if (vm.log)
                console.log(GetDateTime() + " - Timer stopped");
                
                vm.retrieveDataInterval = clearInterval(vm.retrieveDataInterval);
            }

            var serviceLevel = null;
            restFactory.getGlobalParameter('nationalOpsPanelServiceLevel').success(function (data, status, headers, config)
            {
                serviceLevel = data.value;

                if (serviceLevel == null)
                {

                    // setTimeout(function () 
                    // {
                    //     $('#settingsModal').modal('show');
                    // }, 0);
                } 
                else
                {                    
                    vm.serviceLevel = serviceLevel;

                    setBrackets();
                    
                    //TODO Change this to be a single call via the api to get all the branches.
                    restFactory.getBranches().success(function (data, status, headers, config)
                    {
                        if (vm.log)
                        console.log(GetDateTime() + " - branches retrieved");

                        vm.showOpsPanel = true;
                        //vm.branchesData = [];
                        if (data != null && data.length > 0) 
                        {
                            var queuesPromises = [];
                            var servicePointPromises = [];
                            data.forEach(function (branch) {

                                if (!isRefresh) {
                                    if (branch.name != 'Central') { //Multichoice requirement
                                        vm.branchesData.push({
                                            branchId: branch.id,
                                            branchName: branch.name,
                                            openCounters: 0,
                                            servingCounters: 0,
                                            waitTimeMoreThan5: 0,
                                            customersWaiting: 0,
                                            avgTransactionTime: 0,
                                            visits: 0,
                                            serviceLevels: {
                                                lowerBracket: 0, // less than or equal to service level
                                                middleBracket: 0, // more than service level, less than or equal to (service level + 10)
                                                upperBracket: 0, // more than (service level + 10)
                                                historicLowerBracket: 0, // less than or equal to service level
                                                historicMiddleBracket: 0, // more than service level, less than or equal to (service level + 10)
                                                historicUpperBracket: 0 // more than (service level + 10)
                                            }
                                        });
                                    }
                                }

                                //promises
                                var servicePointsPromise = retrieveServicePoints(branch.id);
                                servicePointPromises.push(servicePointsPromise);

                                var queuesPromise = retrieveQueues(branch.id);
                                queuesPromises.push(queuesPromise);                                
                                
                            })

                            $q.all(servicePointPromises).then(function (data) {
                                processServicePoints(data);
                                vm.servicePointsDone = true;
                                
                                if (vm.log)
                                console.log(GetDateTime() + " - processed service points");

                                checkAndStartRetrieveTimer();
                            });

                            $q.all(queuesPromises).then(function (data) {
                                processQueues(data);
                                vm.queuesDone = true;
                                
                                if (vm.log)
                                console.log(GetDateTime() + " - processed queues");

                                checkAndStartRetrieveTimer();
                            });
                        }
                        
                        for (var j = 0 ; j < vm.branchesData.length; j++) 
                        {
                            var branch = vm.branchesData[j];

                            if (branch != null) 
                            {                                        
                                angular.forEach(vm.townAndProvinceData, function (townAndProvinceData, townAndProvinceIndex)
                                {                                    
                                    if(townAndProvinceData.AccentCity == branch.branchName || townAndProvinceData.City == branch.branchName)
                                    {
                                        angular.forEach(vm.tableTemplate.ProvinceData, function (ProvinceData, ProvinceIndex)
                                        {                                            
                                            if(townAndProvinceData.ProvinceName == ProvinceData.Region)
                                            {
                                                //You are more likely to see the error if false visually so this is the reason for not inverting bool.
                                                var foundBranch = false;

                                                angular.forEach(ProvinceData.BranchData, function (tableTemplateBranchData, ProvinceIndex)
                                                {                                                                                                        
                                                    if(branch.branchName == tableTemplateBranchData.branchName)
                                                    {
                                                        if (vm.log)
                                                        console.log(branch.branchName + " found in " + ProvinceData.Region + " Region/Province");
                                                        
                                                        foundBranch = true;
                                                    }                                                    
                                                })
                                                
                                                if(foundBranch == false)
                                                {
                                                    if (vm.log)
                                                    console.log(branch.branchName + " branch added to " + ProvinceData.Region + " Region/Province");

                                                    ProvinceData.BranchData.push(branch);                                                    
                                                    
                                                    ProvinceData.RegionData.openCounters = ProvinceData.RegionData.openCounters + branch.openCounters,
                                                    ProvinceData.RegionData.servingCounters = ProvinceData.RegionData.servingCounters + branch.servingCounters,                                                    
                                                    ProvinceData.RegionData.waitTimeMoreThan5 = ProvinceData.RegionData.waitTimeMoreThan5 + branch.waitTimeMoreThan5,
                                                    ProvinceData.RegionData.customersWaiting = ProvinceData.RegionData.customersWaiting + branch.customersWaiting                                                    
                                                }
                                            }
                                        });
                                    }                                            
                                });
                            }
                        }
                    }).error(function (data, status, headers, config) 
                    {
                        vm.error = data.Message;
                        
                        if (vm.log)
                        console.log('Error: Unable to retrieve branches. Response: ' + status)

                        if (status == 401) {
                            location.href = "/";
                        }
                    });
                }
            }).error(function (data, status, headers, config) 
            {
                // setTimeout(function () {
                //     $('#settingsModal').modal('show');
                // }, 0);
            })
        }
        
        function checkAndStartRetrieveTimer()
        {
            if (vm.servicePointsDone == true &&
                vm.visitsDone == true &&
                vm.queuesDone == true)
            {

                //applyColorRules();

                vm.queuesDone = false;
                vm.servicePointsDone = false;
                vm.visitsDone = false;

                vm.retrieveDataInterval = setInterval(retrieveAllData, 60000, true);
                if (vm.log)
                    console.log(GetDateTime() + " - Timer started");
            }
            else 
            {
                if (vm.log) {
                    console.log(GetDateTime() + " - Queues done: " + vm.queuesDone + " - Visits done: " + vm.visitsDone + " - Service points done: " + vm.servicePointsDone);
                }
            }
        }

        function setBrackets()
        {

            var brackets =
            [
                {
                    text: '< ' + vm.serviceLevel + ' minutes',
                    image: 'green'
                },
                {
                    text: vm.serviceLevel + ' - ' + (parseInt(vm.serviceLevel) + 10) + ' minutes',
                    image: 'orange'
                },
                {
                    text: '> ' + (parseInt(vm.serviceLevel) + 10) + ' minutes',
                    image: 'red'
                }
            ];

            vm.brackets = brackets;
        }

        function retrieveAvgTransactionTime()
        {
            //stop the timer if active
            if (vm.transactionTimeRetrieveInterval != null) {
                vm.transactionTimeRetrieveInterval = clearInterval(vm.transactionTimeRetrieveInterval);
            }

            restFactory.getAvgTransactionTime().success(function (data, status, headers, config) {
                vm.nationalData.avgTransactionTimeSeconds = parseInt(data);
                
                if (vm.log)
                {                    
                    console.log("retrieveAvgTransactionTime data = ");
                    console.log(data);
                    console.log("retrieveAvgTransactionTime parseInt(data) = ");
                    console.log(parseInt(data));
                }                

                vm.transactionTimeRetrieveInterval = setInterval(retrieveAvgTransactionTime, 60000, true);
            }).error(function (data, status, headers, config) {
                console.log('ERROR: Unable to retrieve avg transaction time. Status: ' + status);
            })

            
            retrieveAvgTransactionTimePerBranch();
        }

        function retrieveAvgTransactionTimePerBranch()
        {
            if (vm.branchesData != null && vm.branchesData.length > 0)
            {
                for (var i = 0 ; i < vm.branchesData.length; i++)
                {
                    var branch = vm.branchesData[i];

                    if (branch != null) {

                        $.ajax({
                            url: '/EQWebservices/GetAverageTransactionTimePerBranch?branchId=' + branch.branchId,
                            async: false,
                            dataType: 'text',
                            success: function (data) {
                                if (data != null && data != "") {
                                    branch.avgTransactionTime = parseInt(data);
                                }
                            }
                        });
                    }
                }
            }
        }

        function retrieveServicePoints(branchId)
        {
            var q = $q.defer();

            restFactory.getServicePoints(branchId).success(function (data, status, headers, config) {
                q.resolve(data);
            }).error(function (data, status, headers, config) {
                q.reject('Error retrieving service points. Status: ' + status);
            });

            return q.promise;
        }

        function retrieveQueues(branchId)
        {
            var q = $q.defer();

            restFactory.getQueues(branchId).success(function (data, status, headers, config) {
                q.resolve(data);
            }).error(function (data, status, headers, config) {
                q.reject('Error retrieving queues. Status: ' + status);
            });

            return q.promise;
        }

        function retrieveVisits(branchId, queueId)
        {
            var q = $q.defer();

            restFactory.getVisits(branchId, queueId).success(function (data, status, headers, config) {
                q.resolve(data);
            }).error(function (data, status, headers, config) {
                q.reject('Error retrieving visits. Status: ' + status);
            });

            return q.promise;
        }

        function processServicePoints(branchWithServicePoints)
        {
            if (branchWithServicePoints != null && branchWithServicePoints.length > 0) {

                var nationalOpenCounters = 0;
                var nationalServingCounters = 0;

                branchWithServicePoints.forEach(function (servicePoints) {
                    if (servicePoints[0] != null) {
                        var branchId = servicePoints[0].branchId;
                        var branchOpenCounters = 0;
                        var branchServingCounters = 0;
                        servicePoints.forEach(function (servicePoint) {
                            if (servicePoint.status == 'OPEN') {
                                nationalOpenCounters++;
                                branchOpenCounters++;

                                if (servicePoint.currentVisitId != null) {
                                    nationalServingCounters++;
                                    branchServingCounters++;
                                }
                            }
                        })
                    }

                    for (var j = 0 ; j < vm.branchesData.length; j++) {
                        var branch = vm.branchesData[j];

                        if (branch != null) {
                            if (branch.branchId == branchId) {
                                branch.servingCounters = branchServingCounters;
                                branch.openCounters = branchOpenCounters;
                                break;
                            }
                        }
                    }

                })
                vm.nationalData.openCounters = nationalOpenCounters;
                vm.nationalData.servingCounters = nationalServingCounters;
            }
        }

        function processQueues(allBranchesWithQueues){
            if (allBranchesWithQueues != null && allBranchesWithQueues.length > 0) 
            {
                var visitsPromises = [];
                var visits = 0;
                var nationalCustomersWaiting = 0;
                allBranchesWithQueues.forEach(function (branchQueues) {
                    var branchId = branchQueues[0].branchId;
                    if (branchQueues != null && branchQueues.length > 0) {

                        var branchCustomersWaiting = 0;
                        var branchVisits = 0;

                        branchQueues.forEach(function (queue) {
                            nationalCustomersWaiting = nationalCustomersWaiting + queue.customersWaiting;
                            visits = visits + queue.customersServed;

                            //get all visits for each queue
                            var visitsPromise = retrieveVisits(branchId, queue.id);
                            visitsPromises.push(visitsPromise);
                            vm.visitsPromiseOrder.push(branchId);

                            branchCustomersWaiting = branchCustomersWaiting + queue.customersWaiting;
                            branchVisits = branchVisits + queue.customersServed;
                        })

                        for (var i = 0 ; i < vm.branchesData.length; i++) {
                            var branch = vm.branchesData[i];

                            if (branch != null) {
                                if (branch.branchId == branchId) {
                                    branch.customersWaiting = branchCustomersWaiting;
                                    branch.visits = branchVisits;
                                    break;
                                }
                            }
                        }
                    }
                })

                vm.nationalData.visits = visits;
                vm.nationalData.customersWaiting = nationalCustomersWaiting;

                $q.all(visitsPromises).then(function (data) {
                    var totalWaitingTime = 0;
                    var totalWaitingMoreThan5 = 0;

                    var totalSlLowerBracket = 0;
                    var totalSlMiddleBracket = 0;
                    var totalSlUpperBracket = 0;

                    var historicSlLowerBracket = 0;
                    var historicSlMiddleBracket = 0;
                    var historicSlUpperBracket = 0;

                    var branchWaitingMoreThan5 = 0;

                    var slLowerBracket = 0;
                    var slMiddleBracket = 0;
                    var slUpperBracket = 0;

                    var branchHistoricSlLowerBracket = 0;
                    var branchHistoricSlMiddleBracket = 0;
                    var branchHistoricSlUpperBracket = 0;

                    var previousBranchId = null;

                    for (var i = 0; i < data.length; i++) {
                        var queueWithVisits = data[i];
                        var branchId = vm.visitsPromiseOrder[i];

                        if (branchId != previousBranchId && previousBranchId != null) { //UPDATE BRANCH DETAILS: assuming that when the branch id changes, the previous branch is done processing

                            //this is to determine the waiting times already stored in statdb
                            $.ajax({
                                url: '/EQWebservices/GetWaitingTimePerBranch?branchId=' + previousBranchId,
                                async: false,
                                dataType: 'text',
                                success: function (data) {
                                    if (data != null && data != "") {
                                        var result = data.replace(/^\s+|\s+$/g, '').split(',');

                                        result.forEach(function (item) {
                                            if (item != null && item != '') {
                                                var historicalWaitTime = parseInt(item);

                                                //determine service level brackets
                                                if (historicalWaitTime <= (vm.serviceLevel * 60)) {
                                                    branchHistoricSlLowerBracket++;
                                                    historicSlLowerBracket++;
                                                } else if ((historicalWaitTime > (vm.serviceLevel * 60)) && (historicalWaitTime <= ((vm.serviceLevel * 60) + 600))) {
                                                    branchHistoricSlMiddleBracket++;
                                                    historicSlMiddleBracket++
                                                } else {
                                                    branchHistoricSlUpperBracket++;
                                                    historicSlUpperBracket++;
                                                }
                                            }
                                        })
                                    }
                                }
                            });

                            for (var j = 0 ; j < vm.branchesData.length; j++) {
                                var branch = vm.branchesData[j];
                                
                                if (branch != null) {
                                    if (branch.branchId == previousBranchId) {
                                        branch.waitTimeMoreThan5 = branchWaitingMoreThan5;
                                        branch.serviceLevels.lowerBracket = slLowerBracket;
                                        branch.serviceLevels.middleBracket = slMiddleBracket;
                                        branch.serviceLevels.upperBracket = slUpperBracket;
                                        branch.serviceLevels.historicLowerBracket = branchHistoricSlLowerBracket;
                                        branch.serviceLevels.historicMiddleBracket = branchHistoricSlMiddleBracket;
                                        branch.serviceLevels.historicUpperBracket = branchHistoricSlUpperBracket;

                                        branchHistoricSlLowerBracket = 0;
                                        branchHistoricSlMiddleBracket = 0;
                                        branchHistoricSlUpperBracket = 0;
                                        break;
                                    }
                                }
                            }

                            slLowerBracket = 0;
                            slMiddleBracket = 0;
                            slUpperBracket = 0;

                            branchWaitingMoreThan5 = 0;
                        }

                        queueWithVisits.forEach(function (visit) {
                            totalWaitingTime = totalWaitingTime + visit.waitingTime;

                            //determine waiting more than 5 minutes
                            if (visit.waitingTime > 300) {
                                totalWaitingMoreThan5++;
                                branchWaitingMoreThan5++;
                            }

                            //determine service level brackets
                            if (visit.waitingTime <= (vm.serviceLevel * 60)) {
                                slLowerBracket++;
                                totalSlLowerBracket++;
                            } else if ((visit.waitingTime > (vm.serviceLevel * 60)) && (visit.waitingTime <= ((vm.serviceLevel * 60) + 600))) {
                                slMiddleBracket++;
                                totalSlMiddleBracket++;
                            } else {
                                slUpperBracket++;
                                totalSlUpperBracket++;
                            }
                        })

                        previousBranchId = branchId;
                    }

                    //this is to determine the waiting times already stored in statdb for the last branch in the list
                    $.ajax({
                        url: '/EQWebservices/GetWaitingTimePerBranch?branchId=' + previousBranchId,
                        async: false,
                        dataType: 'text',
                        success: function (data) {
                            if (data != null && data != "") {
                                var result = data.replace(/^\s+|\s+$/g, '').split(',');

                                result.forEach(function (item) {
                                    if (item != null && item != '') {
                                        var historicalWaitTime = parseInt(item);

                                        //determine service level brackets
                                        if (historicalWaitTime <= (vm.serviceLevel * 60)) {
                                            branchHistoricSlLowerBracket++;
                                            historicSlLowerBracket++;
                                        } else if ((historicalWaitTime > (vm.serviceLevel * 60)) && (historicalWaitTime <= ((vm.serviceLevel * 60) + 600))) {
                                            branchHistoricSlMiddleBracket++;
                                            historicSlMiddleBracket++
                                        } else {
                                            branchHistoricSlUpperBracket++;
                                            historicSlUpperBracket++;
                                        }
                                    }
                                })
                            }
                        }
                    });

                    //this continues from UPDATE BRANCH DETAILS. The last branch is not updated when the loop ends, so do it here
                    for (var j = 0 ; j < vm.branchesData.length; j++) {
                        var branch = vm.branchesData[j];

                        if (branch != null) {
                            if (branch.branchId == previousBranchId) {
                                branch.waitTimeMoreThan5 = branchWaitingMoreThan5;
                                branch.serviceLevels.lowerBracket = slLowerBracket;
                                branch.serviceLevels.middleBracket = slMiddleBracket;
                                branch.serviceLevels.upperBracket = slUpperBracket;
                                branch.serviceLevels.historicLowerBracket = branchHistoricSlLowerBracket;
                                branch.serviceLevels.historicMiddleBracket = branchHistoricSlMiddleBracket;
                                branch.serviceLevels.historicUpperBracket = branchHistoricSlUpperBracket;
                                break;
                            }
                        }
                    }

                    restFactory.getAvgWaitingTime().success(function (data, status, headers, config) {
                        var historicAvgWaitingTime = parseInt(data);

                        if (historicAvgWaitingTime > 0) {
                            vm.nationalData.historicAvgWaitingTime = historicAvgWaitingTime;
                        }

                    }).error(function (data, status, headers, config) {

                    });

                    vm.nationalData.totalWaitingTime = totalWaitingTime;
                    vm.nationalData.waitingMoreThan5 = totalWaitingMoreThan5;
                    vm.nationalData.serviceLevels.lowerBracket = totalSlLowerBracket;
                    vm.nationalData.serviceLevels.middleBracket = totalSlMiddleBracket;
                    vm.nationalData.serviceLevels.upperBracket = totalSlUpperBracket;
                    vm.nationalData.serviceLevels.historicLowerBracket = historicSlLowerBracket;
                    vm.nationalData.serviceLevels.historicMiddleBracket = historicSlMiddleBracket;
                    vm.nationalData.serviceLevels.historicUpperBracket = historicSlUpperBracket;

                    drawPieChart();
                });

                for (var j = 0 ; j < vm.branchesData.length; j++) 
                {
                    var branch = vm.branchesData[j];

                    if (branch != null) 
                    {                                        
                        angular.forEach(vm.townAndProvinceData, function (townAndProvinceData, townAndProvinceIndex)
                        {                                    
                            if(townAndProvinceData.AccentCity == branch.branchName || townAndProvinceData.City == branch.branchName)
                            {
                                angular.forEach(vm.tableTemplate.ProvinceData, function (ProvinceData, ProvinceIndex)
                                {                                            
                                    if(townAndProvinceData.ProvinceName == ProvinceData.Region)
                                    {
                                        //You are more likely to see the error if false visually so this is the reason for not inverting bool.
                                        var foundBranch = false;

                                        angular.forEach(ProvinceData.BranchData, function (tableTemplateBranchData, ProvinceIndex)
                                        {                                                                                                        
                                            if(branch.branchName == tableTemplateBranchData.branchName)
                                            {
                                                if (vm.log)
                                                console.log(branch.branchName + " found in " + ProvinceData.Region + " Region/Province");
                                                
                                                foundBranch = true;
                                            }                                                    
                                        })
                                        
                                        if(foundBranch == false)
                                        {
                                            if (vm.log)
                                            console.log(branch.branchName + " branch added to " + ProvinceData.Region + " Region/Province");

                                            ProvinceData.BranchData.push(branch);                                                    
                                            
                                            ProvinceData.RegionData.openCounters = ProvinceData.RegionData.openCounters + branch.openCounters,
                                            ProvinceData.RegionData.servingCounters = ProvinceData.RegionData.servingCounters + branch.servingCounters,                                                    
                                            ProvinceData.RegionData.waitTimeMoreThan5 = ProvinceData.RegionData.waitTimeMoreThan5 + branch.waitTimeMoreThan5,
                                            ProvinceData.RegionData.customersWaiting = ProvinceData.RegionData.customersWaiting + branch.customersWaiting                                                    
                                        }
                                    }
                                });
                            }                                            
                        });
                    }
                }

                vm.visitsDone = true;
                
                if (vm.log)
                console.log(GetDateTime() + " - processed visits");

                checkAndStartRetrieveTimer();

            }
             else
            {
                console.log('Queues are empty.');

                //need to set visits to done here, because the queues are empty, it will not need to process visits
                vm.visitsDone = true;
                
                if (vm.log)
                console.log(GetDateTime() + " - processed visits");

                checkAndStartRetrieveTimer();
            }
        }

        //Alternative to fuse pie chart
        function drawPieChart()
        {            
            // var color = d3.scale.ordinal().range(["#5cb85c", "#f0ad4e", "#FF0000"]);

            // var w = 400,                        //width
            // h = 400,                            //height
            // r = 170,                            //radius
            // ir = 70,
            // pi = Math.PI;
            //color = d3.scale.category20c();

            var total = vm.nationalData.serviceLevels.lowerBracket + vm.nationalData.serviceLevels.middleBracket + vm.nationalData.serviceLevels.upperBracket;

            if (total == 0) {
                lowerValue = 1;
                lowerText = '';
            } else {

                var lowerValue = 0;
                var lowerText = '';
                var lower = parseInt(vm.nationalData.serviceLevels.lowerBracket);
                if (lower > 0) {
                    lowerValue = lower / total * 100;
                    lowerText = Math.round(lowerValue) + '%';
                }

                var middleValue = 0;
                var middleText = '';
                var middle = parseInt(vm.nationalData.serviceLevels.middleBracket);
                if (middle > 0) {
                    middleValue = middle / total * 100;
                    middleText = Math.round(middleValue) + '%';
                }

                var upperValue = 0;
                var upperText = '';
                var upper = parseInt(vm.nationalData.serviceLevels.upperBracket);
                if (upper > 0) {
                    upperValue = upper / total * 100;
                    upperText = Math.round(upperValue) + '%';
                }
            }

            // var myNode = document.getElementById("container1");
            // while (myNode.firstChild) {
            //     myNode.removeChild(myNode.firstChild);
            // }

            vm.pieData = 
            [
                { "label": lowerText, "value": lowerValue },
                { "label": middleText, "value": middleValue },
                { "label": upperText, "value": upperValue }
            ];

            
            if (vm.log)
            {
                console.log("Pie Chart Data = ");
                console.log(vm.pieData);
            }
            
            //Initialize Widget 6
            vm.widget6.init();

            // var vis = d3.select("svg")
            //     .data([data])
            //         .attr("width", w)
            //         .attr("height", h)
            //     .append("svg:g")
            //         .attr("transform", "translate(" + r + "," + r + ")")

            // var arc = d3.svg.arc()
            //     .outerRadius(r)
            //     .innerRadius(ir);

            // var pie = d3.layout.pie()
            //     .value(function (d) { return d.value; })
            //     .sort(null)
            //     .startAngle(-90 * (pi / 180))
            //     .endAngle(90 * (pi / 180));

            // var arcs = vis.selectAll("g.slice")
            //     .data(pie)
            //     .enter()
            //         .append("svg:g")
            //             .attr("class", "slice");

            // arcs.append("svg:path")
            //         .attr("fill", function (d, i) { return color(i); })
            //         .attr("d", arc);

            // arcs.append("svg:text")
            //         .attr("transform", function (d) {

            //             return "translate(" + arc.centroid(d) + ")";
            //         })
            //     .attr("text-anchor", "middle")
            //     .attr("style", "font-size:25px; color:white; font-weight:bold")
            //     .text(function (d, i) { return data[i].label; });
        }

        function setExpanded(tableTemplate, set)
        {
            
            if (vm.log)            
            console.log("tableTemplate.RegionData.expanded = " + set);

            tableTemplate.RegionData.expanded = set;

        }
        
        function init()
        {
            //vm.retrieveIntervalSettings();
            //vm.applyColorRules();
            vm.retrieveAllData(false);
            vm.retrieveAvgTransactionTime();
        }
    
        init();
    }    
})();