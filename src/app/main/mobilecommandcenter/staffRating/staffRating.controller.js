﻿(function ()
{
    'use strict';

    angular
        .module('app.staffRating')
        .controller('StaffRatingController', StaffRatingController);

    function StaffRatingController(Users, $q, $mdDialog, $document) {
       
        var vm = this;
        //Data
        vm.users = Users;
        vm.timelineObj = {};
        vm.timeline = [];
        vm.selectedUser = '';
        vm.listOrder = 'name';
        vm.listOrderAsc = false;
        vm.staffDetail = staffDetail;

        vm.timelineOptions = {
            scrollEl: '#content'
        };
        vm.currentPage = 1;
        vm.totalPages = Math.ceil(vm.timeline.length / 5);
        vm.pauseScroll = false;
        vm.file = {
            name: '',
            size: ''
        };

        vm.files = [];
        vm.srcImage = '';
        // Methods
        vm.loadNextPage = loadNextPage;

        angular.forEach(vm.users, function (value) {

            if (value.avatar == '') {
                vm.srcImage = 'assets/images/avatars/profile.jpg';
            } else {
                vm.srcImage = value.avatar;
            }
            vm.timelineObj = {
                "card": {
                    "template": "app/core/directives/ms-card/templates/template-5/template-5.html",
                    "title": '',
                    "event": value.name,
                    "media": {
                        "image": {
                            "src": vm.srcImage,
                            "alt": "Andrew Ryan"
                        }
                    }
                },
                "icon": "icon-account-box",
                "time": '',
                "event": 'Staff Member'
            };
            vm.timeline.push(vm.timelineObj);
        });

        //////////

        /**
         * Load next page
         * @returns promise
         */
        function loadNextPage() {
            // Create a new deferred object
            var deferred = $q.defer();

            // Increase the current page number
            vm.currentPage = vm.currentPage + 1;

            // Check if we still have pages that we can load
            if (vm.currentPage > vm.totalPages) {
                // Reject the promise
                deferred.reject('No more pages');
            }
            else {
                // Emulate the api call and load new timeline items in
                var pageName = 'timeline.page' + vm.currentPage + '@get';

                msApi.request(pageName, {},

                    // SUCCESS
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            vm.timeline.push(response.data[i]);
                        }

                        // Resolve the promise
                        deferred.resolve(response);
                    },

                    // ERROR
                    function (response) {
                        // Reject the promise
                        deferred.reject(response);
                    }
                );
            }

            return deferred.promise;
        };

        function staffDetail(staffData, e) {
            showStaffRatingDialog(staffData, e);
        };

        function showStaffRatingDialog(staffData, e) {
            $mdDialog.show({
                controller: 'staffRatingDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/main/mobilecommandcenter/staffRating/dialogs/staffRating-dialog.html',
                parent: angular.element($document.body),
                targetEvent: e,
                clickOutsideToClose: true,
                locals: {
                    staffData: staffData,
                    event: e
                }
            });
        }

        console.log(Users);

    }            
})();