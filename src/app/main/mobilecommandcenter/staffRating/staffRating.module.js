﻿(function () {
    'use strict';

    angular
    .module('app.staffRating', ['ngMaterial', 'jkAngularRatingStars'])
    .config(config);

    function config($stateProvider, msApiProvider, msNavigationServiceProvider) {
        $stateProvider
                .state('app.staffRating', {
                    url: '/staffRating',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/staffRating/staffRating.html',
                            controller: 'StaffRatingController as vm'
                        }
                    },
                    resolve: {
                        Users: function (apiResolver)
                            {
                            return apiResolver.resolve('user.getUsers@query');
                            }
                    }
                })

        // Api
        msApiProvider.register('mobile', ['app/data/qmatic/data.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.staffRating', {
            title: 'Staff Rating',
            icon: 'icon-tile-four',
            state: 'app.staffRating',
            weight: 1
        });
    }
})();