(function ()
{
    'use strict';

    angular.module('app.staffRating')
        .controller('staffRatingDialogController', staffRatingDialogController);

    /** @ngInject */
    function staffRatingDialogController($mdDialog, staffData, event, $location, $rootScope)
    {
        var vm = this;

        // Data
        vm.staffData = staffData.card;
        vm.submitRating = submitRating;
        this.rating = null;

        // Methods
        vm.closeDialog = closeDialog;

        //////////

        /**
         * Close the dialog
         */
        function closeDialog() {
            $mdDialog.hide();
        };

        function submitRating() {
            closeDialog()
            $rootScope.staffRating = true;
            $location.path('/survey');
        };

        vm.firstRate = 0;

        vm.onItemRating = function (rating) {
            $rootScope.rating = rating;
        };

    }
})();
