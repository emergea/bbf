﻿(function ()
{
    'use strict';

    angular
        .module('app.mobile')
        .controller('MobileController', MobileController);

    function MobileController(MobileData, $timeout, api, $scope, $rootScope) {
       
        var vm = this;
        //Data
        vm.mobileData = MobileData.data;        
        vm.isShow = false;
        vm.animation = "slide-right";

         //Methods
        vm.setConfig = setConfig;
        vm.getConfig = getConfig();

        //TODO: Update mobileCommandCenterBaseUrl and mongo url,via broadcast, from api
        vm.basicConfig = 
        {   
            "mongoDB": true,
            "orchestraDB": false,
            "central": true,
            "distribution": false,
            "mobileCommandCenterBaseUrl": "http://localhost:3000",
            "mobileCommandCenterMongoUrl": "http://localhost:4000"
        };
        
        $timeout(function (value) {

            vm.isShow = true
        }, 1000);

        function setConfig()
        {
            //If there is no config create one.                    
            api.config.createConfig.post
            (vm.basicConfig,
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.Config = response;
                    
                },
                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        };
        
        function getConfig()
        {
            api.config.get.get
            (
                //Success response
                function (response)
                {
                    console.log(response);
                    vm.Config = response;
                    $scope.$broadcast('get-config',{ "config": vm.Config});
                    if(vm.Config.length == 0)
                    {
                        vm.setConfig();            
                    }
                },
                //Failure response
                function (response) 
                {
                    console.error(response);                
                }
            );
        };

        $rootScope.$on('set-central', function (event, args)
        {
            vm.basicConfig.central = args.central;            
            console.log(args.central);
        });

        $rootScope.$on('set-distribution', function (event, args)
        {
            vm.basicConfig.distribution = args.distribution;            
            console.log(args.distribution);
        });
    }
})();