(function ()
{
    'use strict';

    angular
        .module('app.settings')
        .controller('SettingsController', SettingsController);

    /** @ngInject */
    function SettingsController(SettingsData, FileSaver, Blob, $mdToast, surveyData, api)
    {
        var vm = this;

        // Data
        vm.settings = SettingsData;
        vm.surveys = surveyData;
        vm.loadOption = 'URL';
        vm.staffRating = false;
        vm.selectedSurvey = '';
        vm.saveSettings = saveSettings;
        vm.defaultSettingsID = '';

        //Load saved load option, default survey and staff rating setting
        if (vm.settings.length > 0) {
            angular.forEach(vm.surveys, function (value) {
                if (value._id == vm.settings[0].cfuSettings.defaultSurvey) {
                    vm.selectedSurvey = value;
                    vm.defaultSettingsID = vm.settings[0]._id;
                    return
                }
            });

            vm.staffRating = vm.settings[0].cfuSettings.staffRating;
            vm.loadOption = vm.settings[0].cfuSettings.loadOptions;
        };

        //True = Mongo else URL
        //if (vm.loadOption) {

        //} else {

        //};

        function saveSettings() {

            if (vm.settings.length == 0) {
                api.settings.createSettings.post({ cfuSettings: { loadOptions: vm.loadOption, defaultSurvey: vm.selectedSurvey._id, staffRating: vm.staffRating }},

                    // Success
                    function (response) {
                        var message = 'Settings Created';
                        console.log(response);
                        $mdToast.show({
                            template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                            hideDelay: 7000,
                            position: 'top right',
                            parent: '#content'
                        });
                      

                    },

                    // Error
                    function (response) {
                        console.error(response);
                    }
                );
            } else {
                api.settings.saveSettings.put({ id: vm.defaultSettingsID }, { cfuSettings: { loadOptions: vm.loadOption, defaultSurvey: vm.selectedSurvey._id, staffRating: vm.staffRating } },
                    
                    // Success
                    function (response) {
                        var message = 'Settings Updated';
                        console.log(response);
                        $mdToast.show({
                            template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                            hideDelay: 7000,
                            position: 'top right',
                            parent: '#content'
                        });

                    },

                    // Error
                    function (response) {
                        console.error(response);
                    }
                )
            }
        };

       
    }
})();
