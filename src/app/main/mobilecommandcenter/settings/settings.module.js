(function ()
{
    'use strict';

    angular
        .module('app.settings', ['xeditable', 'ngFileSaver' ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.settings', {
                url: '/settings',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/mobilecommandcenter/settings/settings.html',
                        controller: 'SettingsController as vm'
                    }
                },
                resolve: {
                    SettingsData: function (apiResolver)
                    {
                        return apiResolver.resolve('settings.getSettings@query');
                    },
                    surveyData: function (apiResolver) {
                        return apiResolver.resolve('questionaires.getQuestionaires@query');
                    } 
                    
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/mobilecommandcenter/settings');

        msNavigationServiceProvider.saveItem('qmatic.settings', {
            title    : 'Settings',
            icon     : 'icon-gear',
            state    : 'app.settings',
            /*stateParams: {
                'param1': 'page'
             },*/
            weight   : 2
        });
    }
})();