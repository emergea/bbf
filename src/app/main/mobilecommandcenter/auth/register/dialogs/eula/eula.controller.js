(function ()
{
    'use strict';

    angular
        .module('app.pages.auth.register')
        .controller('eulaDialogController', eulaDialogController);

    /** @ngInject */
    function eulaDialogController($mdDialog,eula, msUtils, api, $scope)
    {
        var vm = this;

        //data
        vm.eula = eula;
        
        //Methods
        vm.saveUserRegistration = saveUserRegistration;

        function saveUserRegistration(result)
        {
            $scope.saveUserRegistration = result;
            $mdDialog.hide();
        }
    }
})();
