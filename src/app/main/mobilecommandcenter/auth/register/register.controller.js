(function ()
{
    'use strict';

    angular
        .module('app.pages.auth.register')
        .controller('RegisterController', RegisterController)
        .controller('GreetingController', GreetingController);

    /** @ngInject */
    function RegisterController(api,Configs,$mdDialog,$scope,$state,$document)
    {
        var vm = this;
        // Data
        //TODO:Change so that it loads every time that the saveUserRegestration is fired.
        vm.users = undefined;
        vm.configs = Configs;
        //TODO: Store EULA in DB?
        vm.eula = 'Lorem ipsum dolor sit amet, est no facer adolescens, eum cu munere fastidii vituperatoribus. Ei eam minimum inciderint, id eos labore tincidunt, est doctus laboramus ex. Vim ea autem minim, vel ut zril eirmod imperdiet, elit inermis ad per. Alia fabellas sit ad, sea democritum conclusionemque ad. Sit an nulla meliore, vix dolorum imperdiet at. Cu sint commune usu, no usu vidit putant.Mei fugit meliore consequat et, intellegat sententiae usu no, in sea posse alienum ancillae. Est ex fastidii assueverit, eum dicam sapientem id. Ea vide mollis scripta pri. Pro tamquam ceteros expetenda id, mollis feugiat ad cum, erant nonumy doming id est. Tota ullum ne vel, in quo option facilisi.In eum hinc tamquam. Reque assum nonumes his an. Pri in clita fabulas, quaerendum liberavisse complectitur usu in. Atqui diceret dissentias mei id. Vix no corpora epicurei, mei nobis voluptua ex. An per dicat vulputate maiestatis, esse natum ignota ex vix, quo cu deseruisse mnesarchum.Has omnium accommodare no, vis nullam integre oblique in. Velit platonem mei no, cum te splendide complectitur. Ad quis populo est, sed te indoctum maiestatis, veri erroribus in ius. Simul tantas ut sed, cu vivendo delicatissimi eam. Eros gubergren sed ei. Aliquam menandri sensibus per ne, eum et argumentum comprehensam, convenire repudiandae te eam. Libris placerat ad eos, an tation mnesarchum vis, singulis scribentur appellantur te nam.Cibo omnesque cu eam, eos ad equidem pertinax nominati, nec no melius menandri. Ut qui graeco eripuit labores, iusto choro offendit ut vim. Eam forensibus efficiantur ad, ea intellegat persequeris qui. Eum an dolores ancillae adipisci, ad soluta alienum aliquando vim, vel ne reque electram intellegam. Odio populo ea mei, eu ius vidit nemore impetus, eum ei euismod assentior.'
        vm.url;
        
        //Data Init        
        vm.isPageValid = true;
        
        //basic fields on the form.
        vm.currentUser =          
        {
            "name": "",
            "email": "",
            "password": "",
            "avatar": "assets/images/avatars/profile.jpg"
        };       

        // Methods
        vm.saveUserRegestration = saveUserRegestration;
        vm.commitUserRegestration = commitUserRegestration;
        vm.showCustomGreeting = showCustomGreeting;
        vm.authenticate = Authenticate;
        vm.openEulaDialog = openEulaDialog;

        function saveUserRegestration(url)
        {

            //check the users in the Db every time we wish to commit a new user.
            api.user.getUsers.get
            (
                //Success response
                function (response)
                {
                    console.log(response);
                    vm.users = response;
                    openEulaDialog();                                                            
                },

                //Failure response
                function (response) 
                {
                    console.error(response);
                    vm.isPageValid = false;
                    ShowAlert('Save User Registration Failure',response, 'Save User Registration Failure', 'ok');
                }
            );

                       
        };

        //Attemp to commit the new user once all the current users are retrieved.
        function commitUserRegestration(EULA)
        {
            if(!EULA)
            {
                vm.isPageValid = false;                
                $state.reload();
                return;
            }

            //Reset page validation status.
            vm.isPageValid = true;

            angular.forEach(vm.users, function(value, key)
            {
                //Check if the user allready exists?                
                if (value.name === vm.form.username && vm.isPageValid == true)
                {
                    vm.isPageValid = false;
                    ShowAlert('User Register Failure',"The username allready exsists.", 'User Register Failure', 'ok');
                    $state.reload();
                    return;// This return exsits the foreach and not saveUserRegestration as a whole, so we need another if.
                }                
            });            

            //Second if to exit the code if there was an existing user found.
            if(!vm.isPageValid)
            {
                return;
            }

            vm.currentUser.name = vm.form.username;
            vm.currentUser.email = vm.form.email;
            vm.currentUser.password = vm.form.password;
            
            if(vm.currentUser == "password" || vm.currentUser == "Password")
            {
                //TODO: Do baisc password complexity test (e.g. longer than 8 char has numbers and special char)                
                vm.isPageValid = false;
                ShowAlert('Cmmmit User Registration Failure',"Please change the password.", 'Cmmmit User Registration Failure', 'ok');
                $state.reload();
                return;            
            }

            api.user.create.post
            (vm.currentUser,
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.currentUser = response;
                    vm.isPageValid = false;
                    $state.go('app.pages_auth_login');
                    return;
                },
                // Error
                function (response)
                {
                    console.error(response);
                    ShowAlert('User Register Failure',response, 'User Register Failure', 'ok');
                    return;
                }
            );
        }

        function showCustomGreeting($event)
        {
            $mdDialog.show
            ({
                targetEvent: $event,
                template:
                    '<md-dialog>' +

                    '  <md-dialog-content>Welcome {{ customerName }}!</md-dialog-content>' +

                    '  <md-dialog-actions>' +
                    '    <md-button ng-click="closeDialog()" class="md-primary">' +
                    '      Log in' +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>',
                controller: 'GreetingController',
                onComplete: afterShowAnimation,
                locals: { customerName: vm.currentUser.name, customer: vm.currentUser, viewmodel : vm  }
            });
        }

        /**
         * Open new eula dialog
         *
         * @param ev
         * @param contact
         */
        function openEulaDialog(ev)
        {
            $mdDialog.show({
                controller         : 'eulaDialogController',
                controllerAs       : 'vm',
                templateUrl: 'app/main/mobilecommandcenter/auth/register/dialogs/eula/eula.html',
                parent             : angular.element($document.find('#content-container')),
                targetEvent        : ev,
                clickOutsideToClose: false,
                locals             : 
                {
                    eula : vm.eula
                },
                scope: $scope,
                preserveScope: true,
                onRemoving : function () 
                {
                    vm.commitUserRegestration($scope.saveUserRegistration);
                }
            });
        }


        function afterShowAnimation(scope, element, options)
        {
           // post-show code here: DOM element focus, etc.
           // TODO: Add some ng-animate to show the regestration action as a celebration. :)
        }

        //TODO: This should form part of the app as a whole, i.e should be accessable by all controllers to display errors.
        function ShowAlert(title, textContent, ariaLabel, ok) {
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog
            $mdDialog.show
            (
                $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(textContent)
                .ariaLabel(ariaLabel)
                .ok(ok)
            );
        };
    }
    
    function GreetingController($scope, $mdDialog, customer, viewmodel, $rootScope, $state, AuthService, AUTH_EVENTS, AuthInterceptor)
    {
        // Assigned from construction <code>locals</code> options...
        $scope.customer = customer;
        console.log($scope.customer);

        $scope.closeDialog = function()
        {
            // Easily hides most recent dialog shown...
            // no specific instance reference is needed.
            viewmodel.authenticate($rootScope, $state, AuthService, customer);
            
        };
    }

    function Authenticate ($rootScope, $state, AuthService, customer)
    {
        AuthService.register(customer);
        //document.location = viewmodel.configs.mobileCommandCenterBaseUrl;
    }

})();