(function ()
{
    'use strict';

    angular
        .module('app.pages.auth.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController(AuthService, Configs, $state)
    {
        // Data
        var vm = this;
        vm.configs = Configs;

        //Methods assignments
        vm.authService = AuthService;
        vm.checkLogin = checkLogin;

        // Methods
        function checkLogin()
        {
            vm.loginUser = {email : vm.form.email, password : vm.form.password }
            vm.authService.login(vm.loginUser);
        }       


        //////////
    }
})();