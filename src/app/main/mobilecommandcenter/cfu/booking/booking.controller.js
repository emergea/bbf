﻿(function ()
{
    'use strict';

    angular
        .module('app.mobile.booking')
        .controller('BookingController', BookingController);

    function BookingController(bookingData, $state) {
       
        var vm = this;

        //Data
        vm.bookingData = BookingData.data;

    }            
})();