(function ()
{
    'use strict';
    angular
        .module('app.mobile')
        .controller('GamesDialogController', GamesDialogController);

    function GamesDialogController($mdDialog, GameData, msUtils, stepIdParam, branchIdParam)
    {
        var vm = this;
        
        console.log(GameData);
        console.log(stepIdParam);
        console.log(branchIdParam);

        //Data assingment
        vm.gameData = GameData;
        vm.branchId = branchIdParam;
        vm.stepId = stepIdParam;

        console.log(vm.gameData.description)

        // Methods
        vm.navigateToGame = NavigateToGame;
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;

        function NavigateToGame()
        {            
            document.location = vm.gameData.url + "?stepId=" + vm.stepId + "&branchId=" + vm.branchId;
        }

        function closeDialog()
        {
            $mdDialog.hide();
        }


    }
}
)();