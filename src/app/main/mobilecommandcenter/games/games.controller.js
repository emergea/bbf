﻿(function ()
{
    'use strict';

    angular
        .module('app.mobile')
        .controller('GamesController', GamesController);

    function GamesController(GamesData, $location, $mdDialog, $document, api)
    {       
        var vm = this;
        
        //url values
        vm.branchId = $location.search().branchIdParam;
        vm.stepId = $location.search().stepIdParam;
        vm.highScore = $location.search().highScore;                
        vm.lastGamePlayed = $location.search().lastGamePlayed;

        //Data assingment        
        vm.gamesData = GamesData.data.gameConfig;//The configuaratble data        
        vm.dbGamesEntry = GamesData.data.dbGamesDataTemplate;//The template to retrieve/create a set of games.
        vm.dbGameEntry = GamesData.data.dbGameDataTemplate;//The template retrieve/update/delete a specific game.        
        
        //Data Init
        vm.dbGamesData = [];//The static game data set retrieved form the db.
        vm.jasonGameSearchResults = [];
        vm.dbGameSearchResults = [];
        vm.isPageValid = true;        

        //methods declaration
        vm.navigateToGame = NavigateToGame;
        vm.findJsonValue = FindJsonValue;
        vm.findDbValue = FindDbValue;
        vm.updateJsonHighscoreValue = UpdateJsonHighscoreValue;
        vm.openGameDialog = OpenGameDialog;
        vm.thisNeedsToBecomeAservice = ThisNeedsToBecomeAservice;
        vm.mobileGameDataApiControllerGet = MobileGameDataApiControllerGet;
        vm.mobileGameDataApiControllerPost = MobileGameDataApiControllerPost;
        vm.mobileGameDataApiControllerDelete = MobileGameDataApiControllerDelete;
        vm.addAllGameConfigEntriesToDb = AddAllGameConfigEntriesToDb;
        vm.updateConfigurableEntriesWithDbEntries = UpdateConfigurableEntriesWithDbEntries;
        vm.mobileGameDataApiControllerPut = MobileGameDataApiControllerPut;
        vm.updateConfigurableEntriesWithDbEntry = UpdateConfigurableEntriesWithDbEntry;
        
        //Update config values from db.      
        vm.mobileGameDataApiControllerGet();        
        
        function NavigateToGame(url, game)
        {            
            document.location = url + "?stepId=" + vm.stepId + "&branchId=" + vm.branchId;
        }
        
        function FindJsonValue(enteredValue)
        {            
            angular.forEach(vm.gamesData, function(value, key)
            {                
                if (value.name === enteredValue)
                {                    
                    vm.jasonGameSearchResults.push(value);                    
                }
            });
        };        

        function UpdateJsonHighscoreValue(enteredValue, dbHighscore)
        {            
            angular.forEach(vm.gamesData, function(value, key)
            {                               
                if (value.name === enteredValue && value.highscoreCount < dbHighscore)
                {                    
                    value.highscoreCount = dbHighscore;
                }
            });
        };

        function FindDbValue(enteredValue)
        {            
            angular.forEach(vm.dbGamesData, function(value, key)
            {                
                if (value.name === enteredValue)
                {                    
                    vm.dbGameSearchResults.push(value);                    
                }
            });
        };

        function OpenGameDialog(ev, game)
        {
            $mdDialog.show({
                controller         : 'GamesDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/mobilecommandcenter/games/dialogs/game/game-dialog.html',
                parent             : angular.element($document.find('#content-container')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    GameData : game,
                    stepIdParam : vm.stepId,
                    branchIdParam : vm.branchId

                }
            });
        }

        function MobileGameDataApiControllerGet()
        {
            //branch visits
            api.mobile.GamesData.get
            (
                //Success response
                function (response)
                {
                    console.log(response);
                    vm.dbGamesData = response;
                    vm.thisNeedsToBecomeAservice();
                },

                //Failure response
                function (response) 
                {
                    console.error(response);
                    vm.isPageValid = false;
                    ShowAlert('Game Failure',response, 'Game Failure', 'ok')
                }
            );
        };

        function MobileGameDataApiControllerPost(params)
        {            
            api.mobile.GamesData.post
            (
                //Params
                {highScore:params.highScore, name: params.name},

                //Success response
                function (response)
                {
                    //a delete through our api does not return a response.
                    //However doing curl delete does.                    
                },

                //Failure response
                function (response)
                {
                    console.error(response);
                    vm.isPageValid = false;
                    ShowAlert('Game Failure',response, 'Game Failure', 'ok')
                }

            );

        };
        
        function MobileGameDataApiControllerPut(params)
        {            
            api.mobile.GameData.put
            (
                //Params
                {gameId: params.gameId},{highScore:params.highScore, name: params.name},

                //Success response
                function (response)
                {
                    console.log(response);                    
                },

                //Failure response
                function (response)
                {
                    console.error(response);
                    vm.isPageValid = false;
                    ShowAlert('Game Failure',response, 'Game Failure', 'ok')
                }

            );

        };

        function MobileGameDataApiControllerDelete(params)
        {            
            api.mobile.GameData.delete
            (
                //Params
                {gameId: params.gameId},{},

                //Success response
                function (response)
                {
                    console.log(response);                    
                },

                //Failure response
                function (response)
                {
                    console.error(response);
                    vm.isPageValid = false;
                    ShowAlert('Game Failure',response, 'Game Failure', 'ok');
                }
            );
        };

        function AddAllGameConfigEntriesToDb()
        {
            //Insert to DB
            for (var i=0; i<vm.gamesData.length; i++)
            {
                vm.dbGamesEntry.highScore = vm.gamesData[i].highscoreCount;
                vm.dbGamesEntry.name = vm.gamesData[i].name;
                vm.mobileGameDataApiControllerPost(vm.dbGamesEntry);
            }
        }

        function UpdateConfigurableEntriesWithDbEntries()
        {
            for (var i=0; i < vm.dbGamesData.length; i++)
            {
                vm.updateJsonHighscoreValue(vm.dbGamesData[i].name, vm.dbGamesData[i].highScore);
            }
        }

        function UpdateConfigurableEntriesWithDbEntry(entry)
        {
            vm.updateJsonHighscoreValue(entry.name, entry.highScore);
        }        

        //TODO: Once basic crud testsing here is done, convert to $Broadcast and service method.
        function ThisNeedsToBecomeAservice()
        {
            if(vm.dbGamesData.length == 0)
            {
                vm.addAllGameConfigEntriesToDb();                
                return;                
            }
            
            //There should only be one entry per game
            if(vm.dbGamesData.length != vm.gamesData.length)        
            {                
                ShowAlert('Config Sync Failure','The config does not match the db: the config and db may be out of sync; brought in sync...', 'Game Failure', 'ok');                
                //Delete From DB
                for (var i=0; i < vm.dbGamesData.length; i++)
                {                    
                    vm.dbGameEntry.gameId = vm.dbGamesData[i]._id;
                    vm.mobileGameDataApiControllerDelete(vm.dbGameEntry);
                }
                
                //we dont mind create the new entries while the old entries are present in the DB,
                // as long the old ones are unique by id, and removed in that fashion.
                vm.addAllGameConfigEntriesToDb();
                return;
            }            

            // Only if the db entries do not loaded the first time(vm.dbGamesData.length == 0)
            // or the db entries need to cleaned/brought in syn (vm.dbGamesData.length != vm.gamesData.length).
            // Do we care about updating the config entries form the db as the db entries are considered newer.
            vm.updateConfigurableEntriesWithDbEntries();

            //Do we NOT need to process a new highscore for the last game the user played?
            if(angular.isUndefined(vm.highScore) == true || angular.isUndefined(vm.lastGamePlayed) == true)
            {                                    
                return;
            }
            
            vm.findJsonValue(vm.lastGamePlayed);

            if(vm.jasonGameSearchResults[0].highscoreCount >= vm.highScore)
            {
                return;
            }

            vm.findDbValue(vm.lastGamePlayed);
            vm.dbGameEntry.gameId = vm.dbGameSearchResults[0]._id;
            vm.dbGameEntry.highScore = vm.highScore;
            vm.dbGameEntry.name = vm.dbGameSearchResults[0].name;
            vm.mobileGameDataApiControllerPut(vm.dbGameEntry);

            vm.dbGamesEntry.highScore = vm.highScore;
            vm.dbGamesEntry.name = vm.dbGameSearchResults[0].name;
            vm.updateConfigurableEntriesWithDbEntry(vm.dbGamesEntry);
            return;
        }

        //TODO: This should form part of the app as a whole, i.e should be accessable by all controllers to display errors.
        function ShowAlert (title, textContent, ariaLabel, ok)
        {            
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog
            $mdDialog.show
            (
                $mdDialog.alert()                
                .clickOutsideToClose(true)
                .title(title)
                .textContent(textContent)
                .ariaLabel(ariaLabel)
                .ok(ok)
            );
        };
    }            
})();