﻿(function () {
    'use strict';

    angular
    .module('app.linearQueueing', [])
    .config(config);

    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) 
    {
        $stateProvider
                .state('app.linearQueueing', {
                    url: '/linearQueueing',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/linearQueueing/linearQueueing.html',
                            controller: 'LinearQueueingController as vm'
                        }
                    },
                    resolve: 
                    {                        
                        Configs: function (apiResolver)
                        {
                            return apiResolver.resolve('config.get@query');
                        }
                    }
                })

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/mobilecommandcenter/linearQueueing');

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.linearQueueing', {
            title: 'linearQueueing',
            icon: 'icon-account-network',
            state: 'app.linearQueueing',
            weight: 1
        });
    }
})();