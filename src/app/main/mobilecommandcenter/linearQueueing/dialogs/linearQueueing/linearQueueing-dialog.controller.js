(function ()
{
    'use strict';

    angular
        .module('app.linearQueueing')
        .controller('linearQueueingDialogController', linearQueueingDialogController);

    /** @ngInject */
    function linearQueueingDialogController($mdDialog, api, $scope, queueMappings, availableServices,servicesToBeAllocated, queuesToBeAllocated)
    {
        var vm = this;        

        // Data
        vm.queueMappings = queueMappings;
        vm.availableServices = availableServices;
        vm.servicesToBeAllocated = servicesToBeAllocated;
        vm.queuesToBeAllocated = queuesToBeAllocated;
        vm.queueToBeAllocated = {};
        vm.serviceToBeAllocated = {};
        vm.newQueueToBeAllocated = "";
        vm.newServiceToBeAllocated = "";        

        //Methods
        vm.MapQueue = MapQueue;
        vm.CreateQueue = CreateQueue;
        vm.CreateService = CreateService;
        vm.CreateNewService = CreateNewService;
        vm.CreateQueueMap = CreateQueueMap;

        vm.queueMappingTemplate =
        {   "queueId": 0,             
            "services": [],
            "waitingTime": 0,
            "customersWaiting": 0,                
            "updated_at": ""
        }
        ;

        vm.queueTemplate =
        {   "name": "",             
            "queueType": "",
            "customersWaiting": 0,
            "waitingTime": 0,
            "updated_at": ""
        }
        ;

        vm.serviceTemplate =         
        {   "internalName": "",             
            "externalName": "",
            "internalDescription": "",
            "externalDescription": "",
            "targetTransactionTime": 0,
            "updated_at": ""
        }
        ;
        
        function MapQueue()
        {
            if(vm.queueToBeAllocated == null || vm.serviceToBeAllocated == null)
            {                
                return;
            }

            vm.queueMappingTemplate.queueId = vm.queueToBeAllocated._id;
            vm.queueMappingTemplate.services.push(vm.serviceToBeAllocated);
            CreateQueueMap();            
        }

        function CreateQueue()
        {
            if (vm.newQueueToBeAllocated == "")
            {                
                return;
            }

            vm.queueTemplate.name = vm.newQueueToBeAllocated;
            vm.queueTemplate.updated_at = new Date();
            CreateNewQueue();            
        }

        function CreateService()
        {
            if (vm.newServiceToBeAllocated == "")
            {                
                return;
            }

            vm.serviceTemplate.internalName = vm.newServiceToBeAllocated
            vm.serviceTemplate.externalName = vm.newServiceToBeAllocated
            vm.serviceTemplate.updated_at = new Date();
            CreateNewService();            
        }

        function CreateQueueMap()
        {
            api.queueMapping.createQueueMapping.post
            (vm.queueMappingTemplate,
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.queueMappings.push(vm.queueMappingTemplate);
                    closeDialog();
                },
                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        };

        function CreateNewQueue()
        {
            api.queues.createQueues.post
            (vm.queueTemplate,
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.queuesToBeAllocated.push(response);
                    vm.newQueueToBeAllocated = "";
                },
                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        }

        function CreateNewService()
        {
            api.services.createServices.post
            (vm.serviceTemplate,
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.servicesToBeAllocated.push(response);
                    vm.newServiceToBeAllocated = "";
                },
                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        }

        /**
         * Close dialog
         */
        function closeDialog()
        {
            $mdDialog.hide();
        }

    }
})();