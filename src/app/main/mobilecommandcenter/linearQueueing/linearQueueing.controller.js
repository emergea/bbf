﻿(function ()
{
    'use strict';

    angular
        .module('app.linearQueueing')
        .controller('LinearQueueingController', LinearQueueingController);

    function LinearQueueingController(Configs, $timeout, api, $scope, $mdDialog, $document, $state, $rootScope)
    {
        var vm = this;

        //Data
        vm.Config = Configs;
        vm.queues = [];
        vm.services = [];
        vm.queueMapping =[];
        vm.selectedServices = [];
        vm.availableServices = [];
        vm.availableServicesQueueIds = [];
        vm.servicesToBeAllocated = [];
        vm.queuesToBeAllocated = [];
        vm.visits = [];
        //You have to inject Math into your scope, if you need to use it as $scope
        $scope.Math = window.Math;
        vm.visitNumber = "";
        vm.lastQueueAlpha = "";
        vm.maxTicketNumberCount = 0;

        //Methods
        vm.CreateVisit = CreateVisit;
        vm.CreateQueue = CreateQueue;
        vm.CreateService = CreateService;
        vm.CreateQueueMapping = CreateQueueMapping;
        vm.CallNext = CallNext;
        vm.OpenLinearQueueingDialog = OpenLinearQueueingDialog;
        vm.AddToSelectedService = AddToSelectedService;
        vm.SaveVisit = SaveVisit;
        vm.GetQueueByIDAndSaveVisit = GetQueueByIDAndSaveVisit;
        vm.updateQueueMapping = updateQueueMapping;
        vm.getQueues = getQueues();
        vm.setAvailableServices = setAvailableServices;
        vm.getServicesToBeAllocated = getServicesToBeAllocated;
        vm.getQueuesToBeAllocated = getQueuesToBeAllocated;
        vm.RemoveVisit = RemoveVisit;
        vm.ClearSelectedServices = ClearSelectedServices;
        vm.updateQueueAndDelete = updateQueueAndDelete;       

        vm.queueTemplate =         
        {   "name": "",             
            "queueType": "",
            "customersWaiting": 0,
            "waitingTime": 0,
            "updated_at": ""
        };

        vm.serviceTemplate =         
        {   "internalName": "",
            "externalName": "",
            "internalDescription": "",
            "externalDescription": "",
            "targetTransactionTime": 0,
            "updated_at": ""
        };

        vm.queueMappingTemplate =         
        {   "queueId": 0,
            "services": [],
            "waitingTime": 0,
            "customersWaiting": 0,                
            "updated_at": ""
        };

        vm.visitTemplate =         
        {   
            "visitNumber": "",
            "visitStartTime": "",
            "visitEndTime": "",
            "customerId": [],
            "QueueData": [],
            "ServiceData": [],
            "servicedUserId": "",
            "updated_at": ""
        };

        //Methods        

        //TODO: We need to change the api calls done here(and wrap them in object sets).

        function getQueues()
        {
            api.queues.getQueues.get
            (
                //Success response
                function (response)
                {
                    console.log(response);
                    vm.queues = response;
                    $scope.$broadcast('get-queues',{ "queues": vm.queues});
                },
                //Failure response
                function (response) 
                {
                    console.error(response);                
                }
            );
        }

        api.services.getServices.get
        (
            //Success response
            function (response)
            {
                console.log(response);
                vm.services = response;
                $scope.$broadcast('get-services',{ "services": vm.services});                
            },

            //Failure response
            function (response) 
            {
                console.error(response);                
            }
        );

        api.queueMapping.getQueueMapping.get
        (
            //Success response
            function (response)
            {
                console.log(response);
                vm.queueMapping = response;
                $scope.$broadcast('get-queueMapping',{ "queueMapping": vm.queueMapping});
                setAvailableServices();
            },

            //Failure response
            function (response) 
            {
                console.error(response);                
            }
        );

        api.visits.getVisits.get
        (
            //Success response
            function (response)
            {
                console.log(response);
                vm.visits = response;                
                $scope.$broadcast('get-visits',{ "visits": vm.visits});
            },

            //Failure response
            function (response) 
            {
                console.error(response);                
            }
        );        
       
        function CreateVisit(services)
        {
            //make sure the supplied services
            angular.forEach(services, function(suppliedValue, suppliedKey)
            {
                angular.forEach(vm.queueMapping, function(queueMappingValue, queueMappingKey)
                {                    
                    angular.forEach(queueMappingValue.services, function(queueMappingServiceValue, queueMappingServiceKey)
                    {
                        if(suppliedValue.internalName == queueMappingServiceValue.internalName)
                        {
                            //Set all queue values before updating.
                            GetQueueByIDAndSaveVisit(queueMappingValue);                            
                        }
                    });
                });
            });            
        }

        function CreateQueue()
        {
            //If there is no config create one.                    
            api.queues.createQueues.post
            (vm.queueTemplate,
                
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.queues.push(response);
                    $scope.$broadcast('get-queues',{ "queues": vm.queues});
                },

                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        }

        function CreateService()
        {
            //If there is no config create one.                    
            api.services.createServices.post
            (vm.serviceTemplate,
                
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.services.push(response);
                    $scope.$broadcast('get-services',{ "services": vm.services});                    
                },

                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        }

        function CreateQueueMapping()
        {
            //If there is no config create one.                    
            api.queueMapping.createQueueMapping.post
            (vm.queueMappingTemplate,
                
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.queueMapping.push(response);
                    $scope.$broadcast('get-queueMapping',{ "queueMapping": vm.queueMapping});                    
                },

                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        }

        function OpenLinearQueueingDialog(ev)
        {
            $mdDialog.show({
                controller         : 'linearQueueingDialogController',
                controllerAs       : 'vm',
                templateUrl: 'app/main/mobilecommandcenter/linearQueueing/dialogs/linearQueueing/linearQueueing-dialog.html',
                parent             : angular.element($document.find('#content-container')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : 
                {
                    queueMappings : vm.queueMapping,
                    availableServices : vm.availableServices,
                    servicesToBeAllocated : vm.servicesToBeAllocated,
                    queuesToBeAllocated : vm.queuesToBeAllocated
                },
                onRemoving: function (event, removePromise)
                {
                    $state.reload();
                }
            });
        }

        /**
         * Save queue
         */
        function updateQueueAndDelete(queue,visitId)
        {            
            //Add contact to db
            api.queues.saveQueues.put({ 'id': queue._id }, queue,

                // Success
                function (response)
                {
                    console.log(response);
                    RemoveVisit(visitId);
                    
                    return;
                },

                // Error
                function (response) 
                {
                    console.error(response);
                    return;
                }
            );            
        }

        function updateQueue(queue)
        {            
            //Add contact to db
            api.queues.saveQueues.put({ 'id': queue._id }, queue,

                // Success
                function (response)
                {
                    console.log(response);
                    getQueues();
                    return;
                },

                // Error
                function (response) 
                {
                    console.error(response);
                    return;
                }
            );            
        }

        /**
         * Save updateQueueMapping
         */
        function updateQueueMapping(queueMapping)
        {            
            //Add contact to db
            api.queues.saveQueues.put({ 'id': queue._id }, queue,

                // Success
                function (response) 
                {
                    console.log(response);
                    return;
                },

                // Error
                function (response) 
                {
                    console.error(response);
                    return;
                }
            );            
        }

        function SaveVisit()
        {
            //If there is no config create one.                    
            api.visits.createVisits.post
            (vm.visitTemplate,
                
                // Success
                function (response)
                {                    
                    console.log(response);
                    vm.visits.push(response);
                    $scope.$broadcast('get-visits',{ "visits": vm.visits});                    
                },

                // Error
                function (response)
                {
                    console.error(response);                        
                }
            );
        }

        function AddToSelectedService(selectedService)
        {
            // console.log(selectedService);
            vm.selectedServices.push(selectedService);            
        }

        function GetQueueByIDAndSaveVisit(queueMapping)
        {            
                api.queues.getQueuesById.get({'id': queueMapping.queueId},
                // Success
                function (response)
                {
                    console.log(response);                    
                    var currentDate = new Date();
                    var date = currentDate;

                    if(response.updated_at != null)
                    {
                        //Start of visit
                        if (response.customersWaiting == 0)
                        {
                            date = currentDate;
                        }
                        else
                        {
                            date = new Date(response.updated_at);
                        }                        
                    }
                    else
                    {
                        date = new Date();
                        response.updated_at = currentDate;
                    }                    
                    
                    var milisecondsDiff = currentDate-date;
                    var secondsDiff = milisecondsDiff/1000;
                    var minutesDiff = secondsDiff/60;
                    
                    //var hoursDiff = minutes/60;                   
                    response.waitingTime = minutesDiff;
                    response.customersWaiting =  response.customersWaiting + 1;
                    vm.visitTemplate.ServiceData = queueMapping.services;
                    vm.visitTemplate.QueueData = response;                    
                    SaveVisit();

                    var count = 0;
                    var indexFound = -1;
                    angular.forEach(vm.queues, function(queuesValue, queuesKey)
                    {
                        if(response._id == queuesValue._id)
                        {
                            indexFound = count;
                        }
                        count ++;
                    });
                    
                    var chr = String.fromCharCode(97 + indexFound).toUpperCase();

                    if(vm.lastQueueAlpha == "")
                    {
                        vm.lastQueueAlpha = chr;
                    }

                    if(vm.visits.length > 0 && vm.lastQueueAlpha == chr)
                    {                                               
                        var lastVisit = {};
                        lastVisit = vm.visits[vm.visits.length-1]
                        
                        vm.lastQueueAlpha = lastVisit.visitNumber.substring(0, 1)
                        var lastVisitDigits = lastVisit.visitNumber.substring(1, 4)
                        var oldString = lastVisitDigits;
                        lastVisitDigits = parseInt(lastVisitDigits) + 1;                        
                        
                        if(lastVisitDigits < 10)
                        {
                            lastVisit.visitNumber = lastVisit.visitNumber.substring(0,3);
                            lastVisit.visitNumber = lastVisit.visitNumber + lastVisitDigits.toString();
                        }
                        else if(lastVisitDigits >= 10 &&  lastVisitDigits < 100)
                        {
                            lastVisit.visitNumber = lastVisit.visitNumber.substring(0,2);
                            lastVisit.visitNumber = lastVisit.visitNumber + lastVisitDigits.toString();
                        }
                        else
                        {
                            lastVisit.visitNumber = lastVisit.visitNumber.substring(0,1);
                            lastVisit.visitNumber = lastVisit.visitNumber + lastVisitDigits.toString();
                        }

                        vm.visitTemplate.visitNumber = lastVisit.visitNumber;
                    }
                    else
                    {
                        vm.visitTemplate.visitNumber = chr.toUpperCase() + "000"                        
                    }
                    
                    updateQueue(response);                    
                },
    
                // Error
                function (response)
                {
                    console.error(response);
                }
            );
        }

        function setAvailableServices()
        {
            angular.forEach(vm.queueMapping, function(queueMappingValue, queueMappingKey)
            {                    
                angular.forEach(queueMappingValue.services, function(queueMappingServiceValue, queueMappingServiceKey)
                {
                    vm.availableServices.push(queueMappingServiceValue);
                });
                vm.availableServicesQueueIds.push(queueMappingValue.queueId);
            });

            getServicesToBeAllocated();
            getQueuesToBeAllocated();
        }

        function getServicesToBeAllocated()
        {            
            angular.forEach(vm.services, function(servicesValue, servicesKey)
            {
                var flagFound = false;
                angular.forEach(vm.availableServices, function(availableServicesValue, availableServicesKey)
                {
                    if(servicesValue.internalName == availableServicesValue.internalName)
                    {
                        flagFound = true;
                    }
                });

                if(!flagFound)
                {
                    vm.servicesToBeAllocated.push(servicesValue);                    
                }
            });            
        }

        function getQueuesToBeAllocated()
        {
            var flagFound = false;
            angular.forEach(vm.queues, function(queuesValue, queuesKey)
            {
                angular.forEach(vm.availableServicesQueueIds, function(availableServicesQueueIdsValue, availableServicesQueueIdsKey)
                {
                    if(queuesValue._id == vm.availableServicesQueueIds)
                    {
                        flagFound = true;
                    }
                });

                if(!flagFound)
                {
                    vm.queuesToBeAllocated.push(queuesValue);
                }
              
            });
        }

        function RemoveVisit(id)
        {
            api.visits.deleteVisit.delete
            (                    
                {'id': id},
                // Success
                function (response)
                {
                    console.log(response);
                    var index = 0;
                    angular.forEach(vm.visits, function(visitValue, visitKey)
                    {
                        if(response._id == visitValue._id)
                        {
                            vm.visits.splice(index, 1);
                            getQueues();
                        }

                        index ++;
                        
                    });
                    
                    
                },
    
                // Error
                function (response)
                {
                    console.error(response);
                }
            );
        }
        
        function CallNext()
        {
            var flagQueueHandled = false;
            //make sure the supplied services
            angular.forEach(vm.visits, function(visitValue, visitKey)
            {
                api.queues.getQueuesById.get
                (                    
                    {'id': visitValue.QueueData[0]._id},
                    // Success
                    function (response)
                    {
                        console.log(response);
                        if(response.customersWaiting > 0 && flagQueueHandled == false)
                        {
                            var date = new Date();
                            var currentDate = new Date();
                            var milisecondsDiff = currentDate-date;
                            var secondsDiff = milisecondsDiff/1000;
                            var minutesDiff = secondsDiff/60;
                            
                            response.updated_at = currentDate;
                            response.waitingTime = minutesDiff;                    
                            
                            response.customersWaiting = response.customersWaiting - 1;
                            updateQueueAndDelete(response, visitValue._id);
                            flagQueueHandled = true;
                            
                        }
                    },
        
                    // Error
                    function (response)
                    {
                        console.error(response);
                    }
                );                
            });
        }

        function ClearSelectedServices()
        {
            vm.selectedServices = [];
        }

        $rootScope.$on('set-maxTicketNumberCount', function (event, args)
        {
            maxTicketNumberCount = args.maxTicketNumberCount;            
            console.log(args.maxTicketNumberCount);
        });

    }
})();