(function ()
{
    'use strict';

    angular
        .module('app.contacts')
        .controller('ContactDialogController', ContactDialogController);

    /** @ngInject */
    function ContactDialogController($mdDialog, Contact, Contacts, User, msUtils, api, $scope)
    {
        var vm = this;

        // Data
        vm.title = 'Edit Contact';
        vm.contact = angular.copy(Contact);
        vm.contacts = Contacts;
        vm.user = User;
        vm.newContact = false;
        vm.allFields = false;

        if ( !vm.contact )
        {
            vm.contact = {
                'name'    : '',
                'lastName': '',
                'avatar'  : 'assets/images/avatars/profile.jpg',
                'nickname': '',
                'company' : '',
                'jobTitle': '',
                'email'   : '',
                'phone'   : '',
                'address' : '',
                'birthday': null,
                'notes'   : ''
            };

            vm.title = 'New Contact';
            vm.newContact = true;
            vm.contact.tags = [];
        }

        // Methods
        vm.addNewContact = addNewContact;
        vm.saveContact = saveContact;
        vm.deleteContactConfirm = deleteContactConfirm;
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;

        //////////

        /**
         * Add new contact
         */
        function addNewContact()
        {
            vm.contacts.unshift(vm.contact);

           //Add contact to db
           api.contacts.addContacts.post(vm.contact,

                // Success
                function (response) {
                    console.log(response);
                },

                // Error
                function (response) {
                    console.error(response);
                }
            );

            closeDialog();
        }

        /**
         * Save contact
         */
        function saveContact()
        {
            
            //Add contact to db
            api.contacts.saveContact.put({ 'id': vm.contact._id }, vm.contact,

                // Success
                function (response) {
                    console.log(response);
                },

                // Error
                function (response) {
                    console.error(response);
                }
            );

            closeDialog();
        }

        /**
         * Delete Contact Confirm Dialog
         */
        function deleteContactConfirm(ev) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure want to delete the contact?')
                .htmlContent('<b>' + vm.contact.name + ' ' + vm.contact.lastName + '</b>' + ' will be deleted.')
                .ariaLabel('delete contact')
                .targetEvent(ev)
                .ok('OK')
                .cancel('CANCEL');

            $mdDialog.show(confirm).then(function () {

                vm.contacts.splice(vm.contacts.indexOf(Contact), 1);

            });
        };

        //Upload avatar
        $scope.stepsModel = [];

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                vm.contact.avatar = e.target.result;
            });
        }

        $scope.imageUpload = function (element) {
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(element.target.files[0]);

        }

        /**
         * Close dialog
         */
        function closeDialog()
        {
            $mdDialog.hide();
        }

    }
})();