(function ()
{
    'use strict';

    angular
        .module('app.contacts',
            [
                // 3rd Party Dependencies
                'xeditable', 'base64'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        $stateProvider.state('app.contacts', {
            url    : '/contacts',
            views  : {
                'content@app': {
                    templateUrl: 'app/main/mobilecommandcenter/contacts/contacts.html',
                    controller : 'ContactsController as vm'
                }
            },
            resolve: {
                Contacts: function (apiResolver)
                {
                    return apiResolver.resolve('contacts.contacts@query');
                },
                User: function (apiResolver)
                {
                    return apiResolver.resolve('contacts.user@query');
                }
            }
        });

        ////contacts from db
        //api.contacts.contacts.query({},
         
        //// Success
        //function (response)
        //{
        //    contactslength = response.data.length
        //    console.log(response);
        //},
            
        //    // Error
        //    function (response)
        //    {
        //        console.error(response);
        //    }
        // );

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/mobilecommandcenter/contacts');

        // Api
        // msApiProvider.register('contacts.contacts', ['app/data/contacts/contacts.json']);
        //msApiProvider.register('contacts.user', ['app/data/contacts/user.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.contacts', {
            title : 'Contacts',
            icon  : 'icon-account-box',
            state: 'app.contacts',
            weight: 10
        });

    }

})();