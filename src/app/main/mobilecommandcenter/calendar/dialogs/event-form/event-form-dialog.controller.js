(function ()
{
    'use strict';

    angular.module('app.calendar')
        .controller('EventFormDialogController', EventFormDialogController);

    /** @ngInject */
    function EventFormDialogController($mdDialog, dialogData, api, $filter)
    {
        var vm = this;

        // Data
        vm.dialogData = dialogData;
        vm.notifications = ['15 minutes before', '30 minutes before', '1 hour before'];

        // Methods
        vm.saveEvent = saveEvent;
        vm.removeEvent = removeEvent;
        vm.closeDialog = closeDialog;
        vm.createAppointment = createAppointment;
        vm.branches = [];
        vm.branch = {};
        vm.resources = [];
        vm.resource = {};
        vm.services = [];
        vm.selectedServices = [];
        vm.items = [];
        vm.showBranch = false;
        vm.showResource = false;
        vm.isIndeterminate = isIndeterminate;
        vm.toggleAll = toggleAll;
        vm.exists = exists;
        vm.toggle = toggle;
        vm.isChecked = isChecked;
        vm.selectedResource = selectedResource;
        vm.selectedBranch = selectedBranch;
        vm.service = {};
        vm.servicesList = [];

        init();

        //API get branches
        api.calendar.getBranches.get
        (
            //Params
            {},

            //Success response
            function (response) {
                vm.branches = response.branchList;

                if (vm.calendarEvent != undefined) {

                    vm.branches.forEach(function (branch) {
                        if (branch.name == vm.calendarEvent.branch) {
                            selectedBranch(branch);
                        }
                    })
                }
            },

            //Failure response
            function (response) {
                console.error(response);
            }
        );

        //API get resources
        api.calendar.getResources.get
        (
            //Params
            {},

            //Success response
            function (response) {
                vm.resources = response.resourceList;

                if (vm.calendarEvent != undefined) {

                    vm.resources.forEach(function (resource) {
                        if (resource.name == vm.calendarEvent.resource) {
                            selectedResource(resource);
                        }
                    })
                }
            },

            //Failure response
            function (response) {
                console.error(response);
            }
        );

        //API get services
        api.calendar.getServices.get
        (
            //Params
            {},

            //Success response
            function (response) {
                vm.services = response.serviceList;

                if (vm.calendarEvent != undefined) {

                    vm.services.forEach(function (service) {
                        vm.calendarEvent.services.forEach(function (service2) {
                            if (service.name == service2.name) {
                                exists(service);
                                toggle(service);
                                vm.isChecked();
                            }
                        })
                    })

                }
            },

            //Failure response
            function (response) {
                console.error(response);
            }
        );


        //////////

        /**
         * Initialize
         */
        function init()
        {
            // Figure out the title
            switch ( vm.dialogData.type )
            {
                case 'add' :
                    vm.dialogTitle = 'Add Event';
                    break;

                case 'edit' :
                    vm.dialogTitle = 'Add Event';
                    break;

                default:
                    break;
            }

            // Edit
            if ( vm.dialogData.calendarEvent )
            {
                // Clone the calendarEvent object before doing anything
                // to make sure we are not going to brake FullCalendar
                vm.calendarEvent = angular.copy(vm.dialogData.calendarEvent);

                // Convert moment.js dates to javascript date object
                if ( moment.isMoment(vm.calendarEvent.start) )
                {
                    vm.calendarEvent.start = vm.calendarEvent.start.toDate();
                }

                if ( moment.isMoment(vm.calendarEvent.end) )
                {
                    vm.calendarEvent.end = vm.calendarEvent.end.toDate();
                }

            }
            // Add
            else
            {
                // Convert moment.js dates to javascript date object
                if ( moment.isMoment(vm.dialogData.start) )
                {
                    vm.dialogData.start = vm.dialogData.start.toDate();
                }

                if ( moment.isMoment(vm.dialogData.end) )
                {
                    vm.dialogData.end = vm.dialogData.end.toDate();
                }

                vm.calendarEvent = {
                    start        : vm.dialogData.start,
                    end          : vm.dialogData.end,
                    notifications: []
                };
            }

            vm.calendarEvent.startTime = $filter('date')(vm.calendarEvent.start, 'HH:mm');
            vm.calendarEvent.endTime = $filter('date')(vm.calendarEvent.end, 'HH:mm');
        }

        var originatorEv;

        this.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        function selectedBranch(branch) {

            vm.branch = branch;
            vm.showBranch = true;
        }

        function selectedResource(resource) {

            vm.resource = resource;
            vm.showResource = true;
        }

        function isChecked() {

            return vm.selectedServices.length === vm.items.length;
        }

        function toggle(item) {

            var idx = vm.selectedServices.indexOf(item);
            if (idx > -1) {
                vm.selectedServices.splice(idx, 1);
            } else { 
                vm.selectedServices.push(item);
            }
        };

        function exists(item) {
            if (vm.selectedServices != undefined){
                return vm.selectedServices.indexOf(item) > -1;
            }
        };

        function toggleAll() {
            if (vm.selectedServices.length === vm.items.length) {
                vm.selectedServices = [];
            } else if (vm.selectedServices.length === 0 || vm.selectedServices.length > 0) {
                vm.selectedServices = vm.items.slice(0);
            }
        };

        function isIndeterminate() {
            return (vm.selectedServices.length !== 0 &&
                vm.selectedServices.length !== vm.items.length);
        };

        /**
         * Save the event
         */
        function saveEvent()
        {
            // Convert the javascript date objects back to the moment.js dates
            var dates = {
                start: moment.utc(vm.calendarEvent.start),
                end: moment.utc(vm.calendarEvent.end)
            };

            var branch = {
                branchId: vm.branch.id,
                branchName: vm.branch.name
            };

            var resource = {
                resourceId: vm.resource.id,
                resourceName: vm.resource.name
            };

            var service = [];
            angular.forEach(vm.selectedServices, function (value) {
                service.push({ "serviceId": value.id, "serviceName": value.name });
            });

            var response = {
                type         : vm.dialogData.type,
                calendarEvent: angular.extend({}, vm.calendarEvent, dates, branch, resource, service)
            };

            createAppointment();

            $mdDialog.hide(response);

        }

        /**
         * Remove the event
         */
        function removeEvent()
        {

            //Delete appointment from db
            api.calendar.deleteAppointments.delete({ 'id': vm.calendarEvent.id },

            // Success
            function (response) {
                console.log(response);
            },

                // Error
                function (response) {
                    console.error(response);
                }
             );

            var response = {
                type         : 'remove',
                calendarEvent: vm.calendarEvent


            };

            $mdDialog.hide(response);
        }

        /**
         * Close the dialog
         */
        function closeDialog()
        {
            $mdDialog.cancel();
        }

        //API - create appointment
        function createAppointment() {

            var newDate = normalizeDate(vm.calendarEvent.start);

            if (vm.calendarEvent.startTime._i == null) {
                var hourArray = normalizeTime(vm.calendarEvent.startTime);
            } else {
                var hourArray = normalizeTime(vm.calendarEvent.startTime._i);
            };

            vm.calendarEvent.start = newDate + ' ' + hourArray;
            console.log(vm.calendarEvent.start);

            var newDate = normalizeDate(vm.calendarEvent.end);

            if (vm.calendarEvent.endTime._i == null) {
                var hourArray = normalizeTime(vm.calendarEvent.endTime);
            } else {
                var hourArray = normalizeTime(vm.calendarEvent.endTime._i);
            };

            vm.calendarEvent.end = newDate + ' ' + hourArray;
            console.log(vm.calendarEvent.end);

            vm.selectedServices.forEach(function (service) {

                vm.service = JSON.parse(angular.toJson(service));
                vm.servicesList.push(vm.service);
                    //= [{
                    //"qpId": vm.service.qpId,
                    //"additionalCustomerDuration": vm.service.additionalCustomerDuration,
                    //"publicEnabled": vm.service.publicEnabled,
                    //"duration": vm.service.duration,
                    //"publicId": vm.service.publicId,
                    //"active": vm.service.active,
                    //"created": vm.service.created,
                    //"name": vm.service.name,
                    //"id": vm.service.id,
                    //"custom": vm.service.custom
                //}]
            })

            console.log("Services", vm.servicesList)

            //API  Create Appointments, GetAppointments
            api.calendar.createAppointment.post
            (
                //Params
                { "timeZoneBranchId": "1" },
                //31
                {
                    "start": new Date(vm.calendarEvent.start).toISOString(),
                    "end": new Date(vm.calendarEvent.end).toISOString(),
                    "allDay": false,
                    "notes": "",
                    "blocking": false,
                    "customers": [
                        //{
                        //    "qpId": 0,
                        //    "firstName": vm.calendarEvent.,
                        //    "lastName": "Nieuwoudt"

                        //}
                    ],
                    "services": vm.servicesList,
                    "title": vm.calendarEvent.title,
                    "resource": { "id": vm.resource.id },
                    "branch": { "id": vm.branch.id}
                    
                },

                //Success response
                function (response) {
                    //console.log(response);
                },

                //Failure response
                function (response) {
                    //console.error(response);
                }
            );

        };

        var normalizeDate = function (dateString) {
            var date = new Date(dateString);
            var normalized = date.getFullYear() + '-' + (("0" + (date.getMonth() + 1)).slice(-2)) + '-' + ("0" + date.getDate()).slice(-2);
            return normalized;
        };

        var normalizeTime = function (dateString) {
            var date = new Date(dateString);
            var normalizedTime = (date.getHours() + 2) + ':' + ("0" + date.getMinutes()).slice(-2);
            return normalizedTime;
        };
    }
})();
