(function ()
{
    'use strict';

    angular
        .module('app.calendar')
        .controller('CalendarController', CalendarController);

    /** @ngInject */
    function CalendarController($mdDialog, $document, Appointment)
    {
        var vm = this;

        // Data
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        vm.appointments = Appointment;
        vm.events = [];

        angular.forEach(vm.appointments.appointmentList, function (value, key) {
            console.log("Services", value.services);
            vm.events.push([{
                id: value.id,
                title: value.title,
                start: new Date(value.start).toISOString(),
                end: new Date(value.end).toISOString(),
                branch: value.branch.name,
                resource: value.resource.name,
                services: value.services
            }]);
        })

        vm.calendarUiConfig = {
            calendar: {
                editable          : true,
                eventLimit        : true,
                header            : '',
                handleWindowResize: false,
                aspectRatio       : 1,
                dayNames          : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                dayNamesShort     : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                viewRender        : function (view)
                {
                    vm.calendarView = view;
                    vm.calendar = vm.calendarView.calendar;
                    vm.currentMonthShort = vm.calendar.getDate().format('MMM');
                },
                columnFormat      : {
                    month: 'ddd',
                    week : 'ddd D',
                    day  : 'ddd M'
                },
                eventClick        : eventDetail,
                selectable        : true,
                selectHelper      : true,
                select            : select
            }
        };

        // Methods
        vm.addEvent = addEvent;
        vm.next = next;
        vm.prev = prev;

        //////////

        /**
         * Go to next on current view (week, month etc.)
         */
        function next()
        {
            vm.calendarView.calendar.next();
        }

        /**
         * Go to previous on current view (week, month etc.)
         */
        function prev()
        {
            vm.calendarView.calendar.prev();
        }

        /**
         * Show event detail
         *
         * @param calendarEvent
         * @param e
         */
        function eventDetail(calendarEvent, e)
        {
            showEventDetailDialog(calendarEvent, e);
        }

        /**
         * Add new event in between selected dates
         *
         * @param start
         * @param end
         * @param e
         */
        function select(start, end, e)
        {
            showEventFormDialog('add', false, start, end, e);
        }

        /**
         * Add event
         *
         * @param e
         */
        function addEvent(e)
        {
            var start = new Date(),
                end = new Date();

            showEventFormDialog('add', false, start, end, e);
        }

        /**
         * Show event detail dialog
         * @param calendarEvent
         * @param e
         */
        function showEventDetailDialog(calendarEvent, e)
        {
            $mdDialog.show({
                controller         : 'EventDetailDialogController',
                controllerAs       : 'vm',
                templateUrl: 'app/main/mobilecommandcenter/calendar/dialogs/event-detail/event-detail-dialog.html',
                parent             : angular.element($document.body),
                targetEvent        : e,
                clickOutsideToClose: true,
                locals             : {
                    calendarEvent      : calendarEvent,
                    showEventFormDialog: showEventFormDialog,
                    event              : e
                }
            });
        }

        /**
         * Show event add/edit form dialog
         *
         * @param type
         * @param calendarEvent
         * @param start
         * @param end
         * @param e
         */
        function showEventFormDialog(type, calendarEvent, start, end, e)
        {
            var dialogData = {
                type         : type,
                calendarEvent: calendarEvent,
                start        : start,
                end          : end
            };

            $mdDialog.show({
                controller         : 'EventFormDialogController',
                controllerAs       : 'vm',
                templateUrl: 'app/main/mobilecommandcenter/calendar/dialogs/event-form/event-form-dialog.html',
                parent             : angular.element($document.body),
                targetEvent        : e,
                clickOutsideToClose: true,
                locals             : {
                    dialogData: dialogData
                }
            }).then(function (response)
            {
                switch ( response.type )
                {
                    case 'add':
                        // Add new
                        vm.events[0].push({
                            id   : vm.events[0].length + 20,
                            title: response.calendarEvent.title,
                            start: response.calendarEvent.start,
                            end: response.calendarEvent.end,
                            branch: response.calendarEvent.branchName,
                            resource: response.calendarEvent.resourceName,
                            services: response.calendarEvent[0]
                        });
                        break;

                    case 'edit':
                        // Edit
                        for ( var i = 0; i < vm.events[0].length; i++ )
                        {
                            // Update
                            if ( vm.events[0][i].id === response.calendarEvent.id )
                            {
                                vm.events[0][i] = {
                                    title: response.calendarEvent.title,
                                    start: response.calendarEvent.start,
                                    end: response.calendarEvent.end,
                                    branch: response.calendarEvent.branch.name,
                                    resource: response.calendarEvent.resource.name,
                                    services: response.calendarEvent.services
                                };

                                break;
                            }
                        }
                        break;

                    case 'remove':
                        // Remove
                        for ( var j = 0; j < vm.events[0].length; j++ )
                        {
                            // Update
                            if ( vm.events[0][j].id === response.calendarEvent.id )
                            {
                                vm.events[0].splice(j, 1);

                                break;
                            }
                        }
                        break;
                }
            });
        }
    }

})();