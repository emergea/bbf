(function ()
{
    'use strict';

    angular
        .module('app.calendar',
            [
                // 3rd Party Dependencies
                'ui.calendar', 'ngMaterialDatePicker'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.calendar', {
            url      : '/booking',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/mobilecommandcenter/calendar/calendar.html',
                    controller : 'CalendarController as vm'
                }
            },
            resolve: {
                Appointment: function (apiResolver)
                {
                    //return msApi.resolve('calendar.temp@get');
                    return apiResolver.resolve('calendar.getAppointments@get');
                }
            },
            bodyClass: 'calendar'
        });

        //For local debugging enable
        //msApiProvider.register('calendar.temp', ['app/data/calendar/appointments.json']);

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/mobilecommandcenter/calendar');

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.calendar', {
            title : 'Calendar',
            icon  : 'icon-calendar-today',
            state: 'app.calendar',
            weight: 2
        });
    }
})();